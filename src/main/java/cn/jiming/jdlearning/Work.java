package cn.jiming.jdlearning;

import ai.djl.modality.cv.Image;
import ai.djl.modality.cv.ImageFactory;
import cn.jiming.jdlearning.face.FaceNet;
import cn.jiming.jdlearning.face.detection.RetinaFaceDetection;
import cn.jiming.jdlearning.face.feature.FeatureExtraction;
import cn.jiming.jdlearning.face.train.FaceClassificationTrain;
import org.bytedeco.javacv.*;
import org.bytedeco.opencv.opencv_core.Mat;

import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static org.bytedeco.opencv.global.opencv_imgcodecs.imwrite;

public class Work {
    private static OpenCVFrameConverter.ToIplImage converter = new OpenCVFrameConverter.ToIplImage();
    private static int w = java.awt.Toolkit.getDefaultToolkit().getScreenSize().width / 2 - 100;
    private static int h = java.awt.Toolkit.getDefaultToolkit().getScreenSize().height / 2;

    public File this_pash = new File(this.getClass().getResource("").getPath());

    public File jd_pash = new File("D:\\Ill\\codes\\JAVA\\test\\java-ee\\foreign-people-manage\\src\\main\\resources\\cn\\jiming\\jdlearning\\train");

    public void capture(String name) {
        OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
        try {
            grabber.start();   //开始获取摄像头数据
            CanvasFrame canvas = new CanvasFrame("摄像头");//新建一个窗口
            canvas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            canvas.setAlwaysOnTop(true);

            int i = 0;
            String dir = this_pash + "\\train";
            System.out.println("work : " + dir);
            File file = new File(dir, name);
            file.mkdirs();
            File file2 = new File(jd_pash, name);
            file2.mkdirs();

            while (true) {
                Frame frame = grabber.grabFrame();
                // 将Frame转为Mat
                Mat mat = converter.convertToMat(frame);

                //人脸定位
                List<Mat> faces = RetinaFaceDetection.faceDetection(mat);

                //保存人脸 （您自己修改路径）
                for (Mat face : faces) {
                    // 输出图片
                    imwrite(dir + "\\" + name + "\\" + i + ".png", face);
                    imwrite(jd_pash + "\\" + name + "\\" + i + ".png", face);
                    i++;
                }

                //Image转Frame
                Frame convertFrame1 = Java2DFrameUtils.toFrame(mat);

                canvas.showImage(convertFrame1);//获取摄像头图像并放到窗口上显示， 这里的Frame frame=grabber.grab(); frame是一帧视频图像
                mat.release();

                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (i > 10) {
                    canvas.dispose();
                    break;
                }
            }
        } catch (FrameGrabber.Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void train() {
        String trainPath = this_pash + "\\train";
        String faceNetPath = this_pash + "\\models\\face_net.xm";

        //开始训练
        System.out.println("***** 开始训练 ***** ");
        FaceClassificationTrain.training(null, trainPath, faceNetPath);
        System.out.println("***** 训练完成 ***** ");
    }


    public String open() {
        // 存储对应人物的识别度, 如果识别度超过0.8就加一
        HashMap<String, Integer> lagMap = new HashMap<>();
        // 取minTime次
        int minTime = 10;
        int i = 0;

        FaceNet faceNet = FaceNet.load(this_pash + "/models/face_net.xm");
        //FaceNet faceNet = FaceNet.load(this_pash + "\\face_net.xm");
        OpenCVFrameGrabber grabber = new OpenCVFrameGrabber(0);
        try {
            grabber.start();   //开始获取摄像头数据
            CanvasFrame canvas = new CanvasFrame("摄像头");//新建一个窗口
            canvas.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            canvas.setAlwaysOnTop(true);
            while (true) {
                if (!canvas.isDisplayable()) {//窗口是否关闭
                    grabber.stop();//停止抓取
                    // System.exit(2);//退出
                    break;
                }
                Frame frame = grabber.grabFrame();
                // 将Frame转为Mat
                Mat mat = converter.convertToMat(frame);
                //mat转换imag
                BufferedImage bimg = Java2DFrameUtils.toBufferedImage(mat);
                //转DJL img
                Image img = ImageFactory.getInstance().fromImage(bimg);
                //人脸定位
                List<Image> faces = RetinaFaceDetection.faceDetection(img, true, mat);//RetinaFace
                //List<Image> faces = FaceDetection.detection(mat);//moble net
                //特征提取
                if (faces != null && faces.size() > 0) {
                    for (Image face : faces) {
                        float[] f = FeatureExtraction.faceFeatureExtraction(face);
                        if (f != null) {
                            //lab推理
                            FaceNet.MaxLab maxLab = faceNet.predict(f);
                            if (maxLab != null && maxLab.maxSimilar > 0.8) {
                                lagMap.put(maxLab.maxLab, lagMap.getOrDefault(maxLab.maxLab, 0) + 1);
                                if (lagMap.get(maxLab.maxLab) >= 8) {
                                    //Image转Frame
                                    BufferedImage bi = (BufferedImage) img.getWrappedImage();
                                    Frame convertFrame1 = Java2DFrameUtils.toFrame(bi);
                                    //	Frame convertFrame1 = Java2DFrameUtils.toFrame(mat);
                                    canvas.showImage(convertFrame1);//获取摄像头图像并放到窗口上显示， 这里的Frame frame=grabber.grab(); frame是一帧视频图像
                                    mat.release();
                                    grabber.stop();//停止抓取
                                    canvas.dispose();
                                    return maxLab.maxLab;
                                }
                            }
                            if (i >= minTime) {
                                //Image转Frame
                                BufferedImage bi = (BufferedImage) img.getWrappedImage();
                                Frame convertFrame1 = Java2DFrameUtils.toFrame(bi);
                                //	Frame convertFrame1 = Java2DFrameUtils.toFrame(mat);
                                canvas.showImage(convertFrame1);//获取摄像头图像并放到窗口上显示， 这里的Frame frame=grabber.grab(); frame是一帧视频图像
                                mat.release();
                                grabber.stop();//停止抓取
                                canvas.dispose();
                                return null;
                            }
                            System.out.println("lab=" + maxLab.maxLab + "  相似度=" + maxLab.maxSimilar);
                            i++;
                        }
                    }
                }
                //Image转Frame
                BufferedImage bi = (BufferedImage) img.getWrappedImage();
                Frame convertFrame1 = Java2DFrameUtils.toFrame(bi);
                //	Frame convertFrame1 = Java2DFrameUtils.toFrame(mat);
                canvas.showImage(convertFrame1);//获取摄像头图像并放到窗口上显示， 这里的Frame frame=grabber.grab(); frame是一帧视频图像
                mat.release();
            }
        } catch (FrameGrabber.Exception e) {
            throw new RuntimeException(e);
        }
        return null;
    }
}
