package com.itheima.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyConfig implements WebMvcConfigurer {
    // 配置虚拟路径访问映射
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // 映射图片保存的地址
        registry.addResourceHandler("/userHead/**")
                .addResourceLocations("file:D:\\Ill\\codes\\JAVA\\test\\java-ee\\foreign-people-manage\\src\\main\\webapp\\userHead");

    }
}
