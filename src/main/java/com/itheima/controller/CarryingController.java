package com.itheima.controller;

import com.itheima.domain.CarryPeople;
import com.itheima.domain.People;
import com.itheima.domain.Relation;
import com.itheima.service.PeopleService;
import com.itheima.service.RelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CarryingController {
    @Autowired
    private PeopleService peopleService;
    @Autowired
    private RelationService relationService;

    // 不同relation代数表示不同的类别;
    private String[] relStr = {"爷爷", "奶奶", "父亲", "母亲", "伴侣","儿子", "女儿", "孙子", "孙女", "非直系亲属"};
    @RequestMapping("/carryingPeopleServlet")
    public String carryingPeople(HttpServletRequest request) {
        System.out.println("进入carryingPeople");
        // 获取本人的数据
        People user_info = (People) request.getSession().getAttribute("USER_INFORMATION");
        System.out.println(user_info);
        // 获取本人相关的携带人口
        List<Relation> this_user_relations = relationService.selectRelationById(user_info.getPid());
        List<People> rel_people = null;
        List<Integer> relations = null;
        List<CarryPeople> carryPeople = null;
        if (this_user_relations != null) {
            for (Relation rel : this_user_relations)
                System.out.println(rel);

            // 用于储存携带的人的信息
            rel_people = new ArrayList<>();
            // 用于储存对应人对应关系
            relations = new ArrayList<>();
            // 用于储存显示的值
            carryPeople = new ArrayList<>();
            for (Relation rel : this_user_relations) {
                People relPeople = peopleService.selectPeopleById(rel.getRel_pid());
                rel_people.add(relPeople);
                relations.add(rel.getRelation());
                carryPeople.add(new CarryPeople(relPeople.getPid(),relPeople.getName(),relPeople.getIdeCard(), relStr[rel.getRelation()]));
            }
            System.out.println("提取数据成功");
        }
        // 存至session中
        request.getSession().setAttribute("rel_people", rel_people);
        request.getSession().setAttribute("relations", relations);
        request.getSession().setAttribute("carryPeople", carryPeople);
        for(CarryPeople cp : carryPeople)
            System.out.println(cp);

        System.out.println("carryingPeople成功!!!");
        return "forward:/jsp/carryingPeople.jsp";
    }

    @RequestMapping("/addRelation")
    public String addRelation(Integer relation, People rel, HttpServletRequest request) {
        if (request.getMethod().equals("GET")) {
            request.setAttribute("index", -1);
            return "forward:/jsp/AddPeopleInformation.jsp";
        }
        System.out.println("这里是addRelation");
        if (rel != null) {
            // 打印接收到的关系人数据
            System.out.println(rel);
            // 添加进people表中
            if(peopleService.selectPeopleByIdeCard(rel.getIdeCard()) == null)
                peopleService.insertPeople(rel);
            // 获取添加到people表中自动生成的id
            int rel_pid = peopleService.selectPeopleByIdeCard(rel.getIdeCard()).getPid();
            // 更新child变量的id
            rel.setPid(rel_pid);
            // 获取session中存储的关系人数据
            List<People> rel_people = null;
            List<Integer> relations = null;
            List<CarryPeople> carryPeople = null;
            if (request.getSession().getAttribute("rel_people") != null) {
                rel_people = (List<People>) request.getSession().getAttribute("rel_people");
                relations = (List<Integer>) request.getSession().getAttribute("relations");
                carryPeople = (List<CarryPeople>) request.getSession().getAttribute("carryPeople");
            } else {
                rel_people = new ArrayList<>();
                relations = new ArrayList<>();
                carryPeople = new ArrayList<>();
                request.getSession().setAttribute("rel_people", rel_people);
                request.getSession().setAttribute("relations", relations);
                request.getSession().setAttribute("carryPeople", carryPeople);
            }

            // 获取用户id
            int pid = ((People) request.getSession().getAttribute("USER_INFORMATION")).getPid();

            // 添加到数据库中
            if(relationService.selectRelationByIdAndRel_pid(pid,rel_pid) == null) {
                relationService.insertRelation(new Relation(pid, rel_pid, relation));
                // 更新session中孩子数据
                rel_people.add(rel);
                relations.add(relation);
                carryPeople.add(new CarryPeople(rel.getPid(),rel.getName(), rel.getIdeCard(), relStr[relation]));
            }else System.out.println("已经有这个关系的信息了");

            System.out.println("relation添加成功");
        } else System.out.println("relation添加识别");
        return "forward:/jsp/carryingPeople.jsp";
    }

    @RequestMapping("/updateRelation")
    public String updateRelation(Integer index,Integer relation, People rel, HttpServletRequest request) {
        if (request.getMethod().equals("GET")) {
            System.out.println("updateRelation: ");
            People r = peopleService.selectPeopleById(rel.getPid());
            System.out.println("rel.getPid() : " + rel.getPid());
            System.out.println(r);
            request.setAttribute("rel",r);
            request.setAttribute("index",index);
            return "forward:/jsp/AddPeopleInformation.jsp";
        }
        if (rel != null) {
            // 打印接收到的关系人数据
            System.out.println(rel);

            // 获取session中存储的关系人数据
            List<People> rel_people = (List<People>) request.getSession().getAttribute("rel_people");
            List<Integer> relations = (List<Integer>) request.getSession().getAttribute("relations");
            List<CarryPeople> carryPeople = (List<CarryPeople>) request.getSession().getAttribute("carryPeople");

            // 更新session中孩子数据
            rel_people.set(index,rel);
            relations.set(index,relation);
            carryPeople.set(index,new CarryPeople(rel.getPid(),rel.getName(), rel.getIdeCard(), relStr[relation]));

            // 获取用户id
            int pid = ((People) request.getSession().getAttribute("USER_INFORMATION")).getPid();

            peopleService.updatePeopleById(rel);
            relationService.updateRelation_Relation_ById(new Relation(pid,rel.getPid(),relation));

            System.out.println("relation更改成功");
        } else System.out.println("relation更改失败");
        return "forward:/jsp/carryingPeople.jsp";
    }
    @RequestMapping("/deleteRelation")
    public String deleteRelation(int index,HttpServletRequest request){
        // 获取session中存储的关系人数据
        List<People> rel_people = (List<People>) request.getSession().getAttribute("rel_people");
        List<Integer> relations = (List<Integer>) request.getSession().getAttribute("relations");
        List<CarryPeople> carryPeople = (List<CarryPeople>) request.getSession().getAttribute("carryPeople");

        int rel_pid = rel_people.get(index).getPid();

        rel_people.remove(index);
        relations.remove(index);
        carryPeople.remove(index);

        // 获取用户id
        int pid = ((People) request.getSession().getAttribute("USER_INFORMATION")).getPid();

        relationService.deleteRelationByIdAndRel_pid(pid,rel_pid);

        return "forward:/jsp/carryingPeople.jsp";
    }
}


