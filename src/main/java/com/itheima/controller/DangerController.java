package com.itheima.controller;

import com.itheima.domain.People;
import com.itheima.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class DangerController {
    @Autowired
    private PeopleService peopleService;

    // 进入预警人员页面的数据获取
    @RequestMapping("/dangerUser")
    public String dangerUser(HttpServletRequest request){
        // 如果crim的session为空
        if(request.getSession().getAttribute("CRIM") == null) {
            // 从数据库中获取数据
            List<People> peopleList = peopleService.selectCrim();
            // 设置session
            request.getSession().setAttribute("CRIM", peopleList);

            // 打印受到的数据
            for (People people : peopleList)
                System.out.println(people);
        }
        return "redirect:/jsp/dangerUser.jsp";
    }

    /**
     * 删除预警人员
     * @param index 在session[crim] 中 该预警人员的位置
     * @param request
     * @return
     */
    @RequestMapping("/delDanger")
    public String delDanger(int index, HttpServletRequest request){
        List<People> crim = (List<People>)request.getSession().getAttribute("CRIM");
        // 获取身份证
        String ide = crim.get(index).getIdeCard();
        // 从session中移除
        crim.remove(index);
        // 更新数据库
        peopleService.updatePeople_IsCrim_ByIdeCard(ide,0);

        return "redirect:/jsp/dangerUser.jsp";
    }

    /**
     * 添加预警人员
     * @param crim 添加人员的信息
     * @param request
     * @return
     */
    @RequestMapping("/addDanger")
    public String addDanger(People crim,HttpServletRequest request){
        System.out.println("addDanger : ");
        if(request.getMethod().equals("GET")){
            // 如果是get请求直接跳转
            return "forward:/jsp/dangerAdd.jsp";
        }

        // 如果获取的数据不为空
        if(crim!=null){
            // 获取session中的crim
            List<People> crims = (List<People>) request.getSession().getAttribute("CRIM");
            // 设置他的被预警状态
            crim.setIsCrim(1);
            System.out.println(crim.getIdeCard());
            // 从people表中获取对应身份证的人员
            People dgp = peopleService.selectPeopleByIdeCard(crim.getIdeCard());
            System.out.println("查找是否已有数据: "+dgp);

            if(dgp == null) {
                // 如果没有该数据,插入该数据
                peopleService.insertPeople(crim);
                crims.add(crim);
            }else{
                // 有数据则更新犯罪状态.
                peopleService.updatePeople_IsCrim_ByIdeCard(crim.getIdeCard(), 1);
                // 若session 中 crims中没有该预警人员的信息.
                if(!crims.contains(crim)){
                    crims.add(dgp);
                }
            }
        }

        return "redirect:/jsp/dangerUser.jsp";
    }

}
