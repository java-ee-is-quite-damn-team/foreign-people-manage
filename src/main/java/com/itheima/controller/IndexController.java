package com.itheima.controller;

import com.itheima.domain.Notice;
import com.itheima.domain.People;
import com.itheima.domain.Users;
import com.itheima.mapper.PeopleMapper;
import com.itheima.service.NoticeService;
import com.itheima.service.PeopleService;
import com.itheima.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class IndexController {
    @Autowired
    private PeopleService peopleService;
    @Autowired
    private NoticeService noticeService;

    @RequestMapping("/index")
    public String notice(HttpServletRequest request) {
        System.out.println("we find it");
        List<Notice> nos = noticeService.selectAllNotice();
        for (Notice no : nos) {
            System.out.println(no);
        }
        request.getSession().setAttribute("NOTICES", nos);
        request.getSession().setAttribute("NOTICES_IS_CHANGE", 0);
        System.out.println("index out");
        return "redirect:/jsp/index.jsp";
    }

    @RequestMapping("/indexAdmin")
    public String indexAdmin(String typeis, int index, HttpServletRequest request) {
        System.out.println(index);
        if (typeis.equals("del")) {
            List<Notice> notices = (List<Notice>) request.getSession().getAttribute("NOTICES");
            int delindex = notices.get(index).getNid();
            notices.remove(index);
            // noticeService.deleteNotice(delindex);
        }
        return "redirect:/jsp/index_admin.jsp";
    }

    @RequestMapping("/addNoice")
    public String addNoice(Notice notice, HttpServletRequest request) {
        if (notice != null) {
            List<Notice> notices = (List<Notice>) request.getSession().getAttribute("NOTICES");
            notices.add(notice);
            noticeService.insertNotice(notice);
        }
        return "redirect:/jsp/index_admin.jsp";
    }

    @RequestMapping("/seeTheNotice")
    public String seeTheNotice(int index, HttpServletRequest request) {
        List<Notice> notices = (List<Notice>) request.getSession().getAttribute("NOTICES");
        request.setAttribute("NOTICE_INFO", notices.get(index));
        return "forward:/jsp/informationDetails.jsp";
    }

    @RequestMapping("/updateNoice")
    public String updateNoice(Integer index, Notice notice, HttpServletRequest request) {
        if (request.getMethod().equals("GET")) {
            Notice thisNotion = ((List<Notice>) request.getSession().getAttribute("NOTICES")).get(index);
            System.out.println(thisNotion.getNid());
            request.setAttribute("thisNotion", thisNotion);
            request.setAttribute("isUpdate", index);
            return "forward:/jsp/informationManager-1.jsp";
        }
        if (notice != null && index != null) {
            System.out.println(notice);
            System.out.println(index);
            List<Notice> notices = (List<Notice>) request.getSession().getAttribute("NOTICES");
            notice.setNid(notices.get(index).getNid());
            notices.set(index, notice);
            noticeService.updateNotice(notice);
        } else System.out.println("no find it");
        return "redirect:/jsp/index_admin.jsp";
    }

}
