package com.itheima.controller;

import com.itheima.domain.People;
import com.itheima.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MarriedController {
    @Autowired
    private PeopleService peopleService;

    /**
     * 初始化Married页面
     *
     * @param request
     * @return
     */
    @RequestMapping("/initMarried")
    public String initMarried(HttpServletRequest request) {
        System.out.println("进入 initMarried");
        // 得到当前登录用户的信息
        People p = (People) request.getSession().getAttribute("USER_INFORMATION");
        // 执行结婚了的操作
        if (p.getIsMarried() == 1) {
            // 根据自己的id去数据库查找是否有伴侣的id
            int mate = peopleService.selectPeopleMate(p.getPid());
            People mateP = null;
            // 如果找到了就进行查找
            if (mate != -1)
                mateP = peopleService.selectPeopleById(mate);
            // 没有找到和找到都设置一个session,键USER_MARRIED传去页面,在页面中进行判断
            request.getSession().setAttribute("USER_MARRIED", mateP);
        }
        // 得到孩子的数据
        List<People> children = peopleService.selectPeopleChild(p.getPid());

        // 有孩子时
        if (children != null) {
            // 设置session,键children
            request.getSession().setAttribute("children", children);
            // 打印
            for (People people : children)
                System.out.println("children: " + people);
        }else request.getSession().setAttribute("children", null);
        return "redirect:/jsp/marrManage.jsp";
    }

    @RequestMapping("/addMate")
    public String addMate(People mate, HttpServletRequest request) {
        // 提交过来的请求是get请求
        if (request.getMethod().equals("GET")) {
            // 打印并跳转至mate设置页面
            System.out.println("get addMate");
            return "forward:/jsp/mateUpdate.jsp";
        }
        // 如果是post请求

        // 如果有伴侣并且后台传来的值不为空, 保证至少输入了身份证和姓名,即数据库中有伴侣其的信息
        if (request.getSession().getAttribute("USER_MARRIED") != null && mate != null && mate.getName() != null && mate.getIdeCard()!=null) {
            // 将其为空的pid赋值
            mate.setPid(((People) request.getSession().getAttribute("USER_MARRIED")).getPid());
            // 更新数据库
            peopleService.updatePeopleById(mate);
            // 更新session中的代表数据
            request.getSession().setAttribute("USER_MARRIED", mate);
        } else if (mate != null) {
            // 即第一次输入伴侣信息时的运行
            System.out.println(mate);
            // 保存到数据库
            peopleService.insertPeople(mate);
            // 保存到session
            request.getSession().setAttribute("USER_MARRIED", mate);
            // 得到用户自己的id
            int pid_1 = ((People) request.getSession().getAttribute("USER_INFORMATION")).getPid();
            // 得到新加老婆的id
            int pid_2 = peopleService.selectPeopleByIdeCard(mate.getIdeCard()).getPid();
            // 添加到couple关系表中
            peopleService.insertCouple(pid_1, pid_2);
            peopleService.insertCouple(pid_2, pid_1);
        }
        return "redirect:/jsp/marrManage.jsp";
    }

    @RequestMapping("/updateChild")
    public String updateChild(Integer choose, People child, HttpServletRequest request) {
        // 如果是页面的跳转
        if (request.getMethod().equals("GET")) {
            // 获取session中孩子的数据
            List<People> children = (List<People>) request.getSession().getAttribute("children");
            // 如果有
            if (children != null) {
                // 获取用户点击的那个子女的数据
                People people = children.get(choose);
                // 设置request属性
                request.setAttribute("child", people);
                request.setAttribute("choose", choose);
            }
            return "forward:/jsp/childUpdate.jsp";
        }
        // 如果是要传值到数据库
        if (child != null) {
            // 打印接收到的孩子数据
            System.out.println(child);
            // 获取session中存储的孩子数据
            List<People> children = (List<People>) request.getSession().getAttribute("children");
            // 更新session中孩子数据
            children.set(choose, child);
            // 更新数据库
            peopleService.updatePeopleByIdeCard(child);
        }

        return "redirect:/jsp/marrManage.jsp";
    }

    @RequestMapping("/addChild")
    public String addChild(People child, HttpServletRequest request) {
        // 如果是页面的跳转
        if (request.getMethod().equals("GET")) {
            // 设置request属性
            request.setAttribute("choose", -1);
            return "forward:/jsp/childUpdate.jsp";
        }
        if (child != null) {
            // 打印接收到的孩子数据
            System.out.println(child);
            // 添加进people表中
            peopleService.insertPeople(child);
            // 获取添加到people表中自动生成的id
            int childId = peopleService.selectPeopleByIdeCard(child.getIdeCard()).getPid();
            // 更新child变量的id
            child.setPid(childId);
            // 获取session中存储的孩子数据
            List<People> children = null;
            if(request.getSession().getAttribute("children") != null)
                children = (List<People>) request.getSession().getAttribute("children");
            else{
                children = new ArrayList<>();
                request.getSession().setAttribute("children",children);
            }
            // 更新session中孩子数据
            children.add(child);
            // 获取用户id
            int pid = ((People)request.getSession().getAttribute("USER_INFORMATION")).getPid();
            // 获取伴侣的id
            int pidmate = ((People)request.getSession().getAttribute("USER_MARRIED")).getPid();
            // 添加进关系表child中

            peopleService.insertChild(pid,childId);
            peopleService.insertChild(pidmate,childId);

            System.out.println("addChild成功!");
        }else System.out.println("addChild失败!");
        return "redirect:/jsp/marrManage.jsp";
    }

}
