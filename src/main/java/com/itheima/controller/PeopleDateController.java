package com.itheima.controller;

import com.itheima.domain.People;
import com.itheima.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class PeopleDateController {
    @Autowired
    private PeopleService peopleService;

    @RequestMapping("/update")
    public String update(People people, HttpServletRequest request){
        System.out.println(people);
        people.setPid(((People)request.getSession().getAttribute("USER_INFORMATION")).getPid());
        if(peopleService.updatePeopleById(people) != 0) request.getSession().setAttribute("isSuccess",1);
        request.getSession().setAttribute("USER_INFORMATION", people);
        return "redirect:/jsp/personData.jsp";
    }
}
