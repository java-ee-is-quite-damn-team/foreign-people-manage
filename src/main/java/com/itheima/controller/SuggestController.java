package com.itheima.controller;

import com.itheima.domain.People;
import com.itheima.domain.Suggest;
import com.itheima.service.SuggestService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class SuggestController {
    @Autowired
    private SuggestService suggestService;

    @RequestMapping("/addSuggest")
    public String addSuggest(Suggest suggest, HttpServletRequest request) {
        if (suggest != null) {
            People people = (People) request.getSession().getAttribute("USER_INFORMATION");
            suggest.setName(people.getName());
            suggest.setIdeCard(people.getIdeCard());
            suggestService.insertSuggest(suggest);
        }
        return "redirect:/jsp/message.jsp";
    }

    @RequestMapping("/initManage")
    public String initManage(HttpServletRequest request) {
        List<Suggest> list = suggestService.selectAllSuggest();
        for (Suggest suggest : list)
            System.out.println(suggest);
        request.getSession().setAttribute("SUGGESTS", list);

        return "redirect:/jsp/message_admin.jsp";
    }
}
