package com.itheima.controller;

import cn.jiming.jdlearning.Work;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class Test {

    @RequestMapping("/test")
    public String Test(String testttt,HttpServletRequest request){
        HttpSession session = request.getSession();

        Work work = new Work();
        work.capture(testttt);
        work.train();
        work.open();

        try {
            /*
            用户账号和密码是否查询出用户信息
                是：将用户信息存入Session，并跳转到后台首页
                否：Request域中添加提示信息，并转发到登录页面
             */
            System.out.println("ss");
            if("ddd"!=null){
                request.getSession().setAttribute("USER_SESSION","sss");
                return "redirect:/jsp/Test.jsp";
            }
            request.setAttribute("msg","用户名或密码错误");
            return  "forward:/jsp/Test.jsp";
        }catch(Exception e){
            e.printStackTrace();
            request.setAttribute("msg","系统错误");
            return  "forward:/jsp/Test.jsp";
        }

        /*
            try {
            people p = peopleService.getPeopleByIdeCard("320104200204036753");
            /*
            用户账号和密码是否查询出用户信息
                是：将用户信息存入Session，并跳转到后台首页
                否：Request域中添加提示信息，并转发到登录页面
             */
        /*  System.out.println(p);
        if(p!=null){
            request.getSession().setAttribute("USER_SESSION",p);
            return "redirect:/admin/Test.jsp";
        }
        request.setAttribute("msg","用户名或密码错误");
        return  "forward:/admin/Test.jsp";
    }catch(Exception e){
        e.printStackTrace();
        request.setAttribute("msg","系统错误");
        return  "forward:/admin/Test.jsp";
    }
         */
    }

}
