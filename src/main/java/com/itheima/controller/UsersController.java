package com.itheima.controller;

import cn.jiming.jdlearning.Work;
import com.itheima.domain.Notice;
import com.itheima.domain.People;
import com.itheima.domain.Users;
import com.itheima.service.PeopleService;
import com.itheima.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import util.MD5;
import util.PasswordManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Controller
public class UsersController {
//    @RequestMapping("/toMainPage")
//    public String toMainPage() {
//        return "index";
//    }

    //注入userService
    @Autowired
    private UsersService usersService;
    @Autowired
    private PeopleService peopleService;

    public String dangerName = null;

    /*
   用户登录
    */
    @RequestMapping("/login")
    public String login(Users user, HttpServletRequest request) {
        try {
            user.setPassword(MD5.encryptMD5(user.getPassword()));
            // user.setPassword(PasswordManager.hashPassword(user.getPassword()));
            Users u = usersService.login(user);
            /*
            用户账号和密码是否查询出用户信息
                是：将用户信息存入Session，并跳转到后台首页
                否：Request域中添加提示信息，并转发到登录页面
             */
            if (u != null) {
                System.out.println("we find it!");
                People p = peopleService.selectPeopleByIdeCard(user.getIdeCard());
                request.getSession().setAttribute("USER_SESSION", u);
                request.getSession().setAttribute("USER_INFORMATION", p);

                if (p.getIsCrim() == 1) {
                    dangerName = p.getName();
                    request.getSession().setAttribute("isDangerPerson", 1);
                } else request.getSession().setAttribute("isDangerPerson", 0);

                if (u.getRec() == 0) {
                    List<Users> glym = usersService.selectALLSLY();
                    request.getSession().setAttribute("beSend", glym);
                    return "redirect:/jsp/index.jsp";
                } else {
                    List<Users> glym = usersService.selectALLPT();
                    request.getSession().setAttribute("beSend", glym);
                    return "redirect:/initUserManage/";
                }
            }
            System.out.println("We are loser!");
            request.setAttribute("msg", "用户名或密码错误");
            return "forward:/jsp/Login.jsp";
        } catch (Exception e) {
            e.printStackTrace();
            request.setAttribute("msg", "系统错误");
            return "forward:/jsp/Login.jsp";
        }
    }

    // 用户注册
    @RequestMapping("/reg")
    public String reg(String ideCard, String password, String userName, HttpServletRequest request) {
        System.out.println(ideCard);
        System.out.println(password);
        System.out.println(userName);
        if (ideCard != null && ideCard != "" && password != null && password != "" && userName != null && userName != "") {
            Users users = new Users();
            People people = new People();
            String pwd = MD5.encryptMD5(password);
            users.setPassword(pwd);
            users.setIdeCard(ideCard);
            users.setRec(0);
            people.setName(userName);
            people.setIdeCard(ideCard);
            System.out.println(users);
            System.out.println(people);

            // 将用户的数据插入数据库
            peopleService.insertPeople(people);
            usersService.insertUser(users);
            return "redirect:/jsp/Login.jsp";
        } else {
            return "forward:/jsp/Login_1.jsp";
        }
    }

    // 用户点击人脸识别
    @RequestMapping("/giveYourFace")
    public String giveYourFace(HttpServletRequest request) {
        if (request.getSession().getAttribute("USER_SESSION") == null) {
            System.out.println("giveYourFace");
            Work work = new Work();
            work.train();
            String ideCard = work.open();
            if (ideCard != null) {
                System.out.println("we find it!");
                Users u = usersService.selectUserByIdeCard(ideCard);
                if (u == null) {
                    System.out.println("没有该用户注册信息请登录");
                    return "redirect:/jsp/Login.jsp";
                }
                People p = peopleService.selectPeopleByIdeCard(ideCard);
                request.getSession().setAttribute("USER_SESSION", u);
                request.getSession().setAttribute("USER_INFORMATION", p);

                if (p.getIsCrim() == 1) {
                    dangerName = p.getName();
                    request.getSession().setAttribute("isDangerPerson", 1);
                } else request.getSession().setAttribute("isDangerPerson", 0);

                if (u.getRec() == 0) {
                    List<Users> glym = usersService.selectALLSLY();
                    request.getSession().setAttribute("beSend", glym);
                    return "redirect:/jsp/index.jsp";
                } else {
                    List<Users> glym = usersService.selectALLPT();
                    request.getSession().setAttribute("beSend", glym);
                    return "redirect:/initUserManage/";
                }
            } else {
                System.out.println("没有找到相关人脸");
                return "redirect:/jsp/Login.jsp";
            }
        }
        return "redirect:/jsp/index.jsp";
    }

    @RequestMapping("/getYourFace")
    public String getYourFace(HttpServletRequest request) {
        System.out.println("getYourFace");
        String ideCard = ((People) request.getSession().getAttribute("USER_INFORMATION")).getIdeCard();
        Work work = new Work();
        work.capture(ideCard);
        work.train();
        System.out.println("获取成功!!!");
        return "redirect:/jsp/personData.jsp";
    }


//    @RequestMapping("/updateNotice")
//    public String updateNotice(Notice notice, HttpServletRequest request) {
//        return "redirect:/jsp/";
//    }

    /*
    注销登录
     */
//    @RequestMapping("/logout")
//    public String logout(HttpServletRequest request) {
//        try {
//            HttpSession session = request.getSession();
//            //销毁Session
//            session.invalidate();
//            return "forward:/jsp/Login.jsp";
//        } catch (Exception e) {
//            e.printStackTrace();
//            request.setAttribute("msg", "系统错误");
//            return "forward:/jsp/Login.jsp";
//        }
//    }
}