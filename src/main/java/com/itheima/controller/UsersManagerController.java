package com.itheima.controller;

import com.itheima.domain.People;
import com.itheima.domain.Users;
import com.itheima.service.PeopleService;
import com.itheima.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.List;

@Controller
public class UsersManagerController {
    @Autowired
    private UsersService usersService;
    @Autowired
    private PeopleService peopleService;

    @RequestMapping("/initUserManage")
    public String initUserManage(HttpServletRequest request) {

        List<People> peopleList = peopleService.selectAllPeople();
        List<Users> users = usersService.selectALLUser();

        for (People people : peopleList)
            System.out.println(people);
        for (Users users1 : users)
            System.out.println(users1);

        HashSet<String> set = new HashSet<>();
        for (Users user1 : users) if (user1.getRec() == 1) set.add(user1.getIdeCard());
        int i = 0;
        while (i < peopleList.size()) {
            if (set.contains(peopleList.get(i).getIdeCard())) {
                peopleList.remove(i);
            } else i++;
        }

        for (People people : peopleList)
            System.out.println(people);

        request.getSession().setAttribute("USERS", peopleList);

        return "redirect:/jsp/user_manager.jsp";
    }

    @RequestMapping("/userDel")
    public String userDel(int index, HttpServletRequest request) {
        List<People> peopleList = (List<People>) request.getSession().getAttribute("USERS");
        String ide = peopleList.get(index).getIdeCard();
        int id = peopleList.get(index).getPid();
        peopleList.remove(index);
        if (usersService.selectUserByIdeCard(ide) != null) {
            usersService.deleteUsersByIdeCard(ide);
        }
        peopleService.deleteCouple(id);
        peopleService.deletePeopleById(id);
        usersService.deleteUsersByIdeCard(ide);
        return "redirect:/jsp/user_manager.jsp";
    }


}
