//package com.itheima.domain;
//
//
//import java.sql.*;
//import java.util.ArrayList;
//import java.util.List;
//
//public class AddPeopleDAO {
//    private String jdbcURL = "jdbc:mysql://localhost:3306/foreignpeoplemanage";
//    private String jdbcUsername = "root";
//    private String jdbcPassword = "root";
//
//    private static final String INSERT_PEOPLE_SQL = "INSERT INTO addpeople (name, sex, tel, rel, card, place, nowplace) VALUES (?, ?, ?, ?, ?, ?, ?)";
//    private static final String SELECT_ALL_PEOPLE = "SELECT * FROM addpeople";
//
//    protected Connection getConnection() {
//        Connection connection = null;
//        try {
//            Class.forName("com.mysql.jdbc.Driver");
//            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        return connection;
//    }
//
//    public void addPerson(Addpeople person) {
//        try (Connection connection = getConnection();
//             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_PEOPLE_SQL)) {
//            preparedStatement.setString(1, person.getName());
//            preparedStatement.setInt(2, person.getSex());
//            preparedStatement.setString(3, person.getTel());
//            preparedStatement.setString(4, person.getRel());
//            preparedStatement.setString(5, person.getCard());
//            preparedStatement.setString(6, person.getPlace());
//            preparedStatement.setString(7, person.getNowplace());
//            preparedStatement.executeUpdate();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public List<Addpeople> getAllPeople() {
//        List<Addpeople> people = new ArrayList<>();
//        try (Connection connection = getConnection();
//             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_PEOPLE)) {
//            ResultSet rs = preparedStatement.executeQuery();
//            while (rs.next()) {
//                int pid = rs.getInt("pid");
//                String name = rs.getString("name");
//                int sex = rs.getInt("sex");
//                String tel = rs.getString("tel");
//                String rel = rs.getString("rel");
//                String card = rs.getString("card");
//                String place = rs.getString("place");
//                String nowplace = rs.getString("nowplace");
//                people.add(new Addpeople(pid, name, sex, tel, rel, card, place, nowplace));
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return people;
//    }
//}
