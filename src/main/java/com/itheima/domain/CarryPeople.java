package com.itheima.domain;

public class CarryPeople {
    private Integer pid;
    private String name;
    private String ideCard;
    private String relation;

    @Override
    public String toString() {
        return "CarryPeople{" +
                "pid=" + pid +
                ", name='" + name + '\'' +
                ", ideCard='" + ideCard + '\'' +
                ", relation='" + relation + '\'' +
                '}';
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public CarryPeople(int pid, String name, String ideCard, String relation) {
        this.pid = pid;
        this.name = name;
        this.ideCard = ideCard;
        this.relation = relation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdeCard() {
        return ideCard;
    }

    public void setIdeCard(String ideCard) {
        this.ideCard = ideCard;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }
}
