package com.itheima.domain;

import java.io.Serializable;
import java.util.Objects;

public class People implements Serializable {
    private Integer pid;            // id
    private String ideCard;           //身份证号
    private String name;              //姓名
    private Integer sex;               //性别
    private String telph;             //电话
    private Integer naPla1;             //籍贯
    private Integer naPla2;             //籍贯
    private Integer adres1;             //地址
    private Integer adres2;             //地址
    private String email;             //生日
    private String eduBack;             //教育背景
    private String health;              //健康情况
    private String work;                //工作情况
    private Integer isMarried;          //是否结婚
    private Integer isArmy;             //是否服过兵役
    private Integer isCrim;                // 1 or 0,标识是否犯罪，1为犯罪，0为无罪


    private String Headimg;

    @Override
    public String toString() {

        return "People{" +
                "pid=" + pid +
                ", ideCard='" + ideCard + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", telph='" + telph + '\'' +
                ", naPla1=" + naPla1 +
                ", naPla2=" + naPla2 +
                ", adres1=" + adres1 +
                ", adres2=" + adres2 +
                ", email='" + email + '\'' +
                ", eduBack='" + eduBack + '\'' +
                ", health='" + health + '\'' +
                ", work='" + work + '\'' +
                ", isMarried=" + isMarried +
                ", isArmy=" + isArmy +
                ", isCrim=" + isCrim +

                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof People)) return false;
        People people = (People) o;
        return Objects.equals(getIdeCard(), people.getIdeCard())|| Objects.equals(getTelph(), people.getTelph()) || Objects.equals(getEmail(), people.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPid(), getIdeCard(), getName(), getSex(), getTelph(), getNaPla1(), getNaPla2(), getAdres1(), getAdres2(), getEmail(), getEduBack(), getHealth(), getWork(), getIsMarried(), getIsArmy(), getIsCrim(), getHeadimg());
    }

    public String getHeadimg() {
        return Headimg;
    }

    public void setHeadimg(String headimg) {
        Headimg = headimg;
    }


    public Integer getNaPla1() {
        return naPla1;
    }

    public void setNaPla1(Integer naPla1) {
        this.naPla1 = naPla1;
    }

    public Integer getNaPla2() {
        return naPla2;
    }

    public void setNaPla2(Integer naPla2) {
        this.naPla2 = naPla2;
    }

    public Integer getAdres1() {
        return adres1;
    }

    public void setAdres1(Integer adres1) {
        this.adres1 = adres1;
    }

    public Integer getAdres2() {
        return adres2;
    }

    public void setAdres2(Integer adres2) {
        this.adres2 = adres2;
    }

    public String getEduBack() {
        return eduBack;
    }

    public void setEduBack(String eduBack) {
        this.eduBack = eduBack;
    }

    public String getHealth() {
        return health;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getIdeCard() {
        return ideCard;
    }

    public void setIdeCard(String ideCard) {
        this.ideCard = ideCard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getTelph() {
        return telph;
    }

    public void setTelph(String telph) {
        this.telph = telph;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIsMarried() {
        return isMarried;
    }

    public void setIsMarried(Integer isMarried) {
        this.isMarried = isMarried;
    }

    public Integer getIsArmy() {
        return isArmy;
    }

    public void setIsArmy(Integer isArmy) {
        this.isArmy = isArmy;
    }

    public Integer getIsCrim() {
        return isCrim;
    }

    public void setIsCrim(Integer isCrim) {
        this.isCrim = isCrim;
    }
}
