package com.itheima.domain;

public class Relation {
    private int pid;        // 本人
    private int rel_pid;    // 与本人有关系的人
    private int relation;   // 关系

    public Relation(int pid, int rel_pid, int relation) {
        this.pid = pid;
        this.rel_pid = rel_pid;
        this.relation = relation;
    }

    @Override
    public String toString() {
        return "Relation{" +
                "pid=" + pid +
                ", rel_pid=" + rel_pid +
                ", relation=" + relation +
                '}';
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getRel_pid() {
        return rel_pid;
    }

    public void setRel_pid(int rel_pid) {
        this.rel_pid = rel_pid;
    }

    public int getRelation() {
        return relation;
    }

    public void setRelation(int relation) {
        this.relation = relation;
    }
}
