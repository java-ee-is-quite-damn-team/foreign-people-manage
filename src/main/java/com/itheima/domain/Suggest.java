package com.itheima.domain;

public class Suggest {
    private int sid;
    private String name;
    private String ideCard;
    private String context;

    @Override
    public String toString() {
        return "Suggest{" +
                "sid=" + sid +
                ", name='" + name + '\'' +
                ", ideCard='" + ideCard + '\'' +
                ", context='" + context + '\'' +
                '}';
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdeCard() {
        return ideCard;
    }

    public void setIdeCard(String ideCard) {
        this.ideCard = ideCard;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }
}
