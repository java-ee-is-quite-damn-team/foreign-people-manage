package com.itheima.domain;

import java.io.Serializable;
import java.util.Objects;

public class Users implements Serializable {
    private String ideCard; //身份证号码
    private String password;   //密码
    private Integer rec; //1 or 0 1是管理员 0是普通用户

    private Integer id;

    @Override
    public String toString() {
        return "Users{" +
                "ideCard='" + ideCard + '\'' +
                ", password='" + password + '\'' +
                ", rec=" + rec +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Users)) return false;
        Users users = (Users) o;
        return Objects.equals(getIdeCard(), users.getIdeCard()) && Objects.equals(getPassword(), users.getPassword()) && Objects.equals(getRec(), users.getRec()) && Objects.equals(getId(), users.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIdeCard(), getPassword(), getRec(), getId());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdeCard() {
        return ideCard;
    }

    public void setIdeCard(String ideCard) {
        this.ideCard = ideCard;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRec() {
        return rec;
    }

    public void setRec(Integer rec) {
        this.rec = rec;
    }
}
