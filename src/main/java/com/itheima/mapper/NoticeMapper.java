package com.itheima.mapper;

import com.itheima.domain.Notice;
import com.itheima.domain.Users;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface NoticeMapper {
    @Select("select * from notice where nid=#{nid} ")
    Notice selectNoticeById(int nid);

    @Select("select * from notice")
    List<Notice> selectAllNotice();

    @Insert("insert into notice values(#{nid},#{title},#{workPlace},#{time},#{adrs},#{type},#{context})")
    int insertNotice(Notice notice);

    @Update("update notice set title=#{title},workPlace=#{workPlace},time=#{time},adrs=#{adrs},type=#{type},context=#{context} where nid=#{nid}")
    int updateNotice(Notice notice);

    @Update("update notice set title=#{title} where nid=#{nid}")
    int updateNotice_title_ById(int nid,String title);

    @Update("update notice set workPlace=#{workPlace} where nid=#{nid}")
    int updateNotice_workPlace_ById(int nid,String workPlace);

    @Update("update notice set time=#{time} where nid=#{nid}")
    int updateNotice_time_ById(int nid,String time);

    @Update("update notice set adrs=#{adrs} where nid=#{nid}")
    int updateNotice_adrs_ById(int nid,String adrs);

    @Update("update notice set type=#{type} where nid=#{nid}")
    int updateNotice_type_ById(int nid,String type);

    @Update("update notice set context=#{context} where nid=#{nid}")
    int updateNotice_context_ById(int nid,String context);

    @Delete("delete from notice where nid=#{nid}")
    int deleteNotice(int nid);
}
