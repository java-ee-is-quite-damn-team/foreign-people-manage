package com.itheima.mapper;

import com.itheima.domain.People;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface PeopleMapper {
    @Select("select * from people where pid=#{pid}")
    People selectPeopleById(int pid);

    @Select("select * from people where ideCard=#{ideCard}")
    People selectPeopleByIdeCard(String ideCard);

    @Select("select * from crim")
    List<People> selectCrim();

    @Select("select * from people")
    List<People> selectAllPeople();

    @Select("select pid_2 from couple where pid_1=#{pid}")
    Object selectPeopleMate(int pid);

    @Select("select children from child where pid=#{pid}")
    List<Integer> selectPeopleChild(int pid);

    @Insert("insert into people(ideCard,name,sex,telph,napla1,napla2," +
            "adres1,adres2,email,eduBack,isMarried,isArmy,health,work,isCrim)" +
            " values (#{ideCard},#{name},#{sex}," +
            "#{telph},#{naPla1},#{naPla2},#{adres1}," +
            "#{adres2},#{email},#{eduBack},#{isMarried},#{isArmy}," +
            "#{health},#{work},#{isCrim})")
    int insertPeople(People people);

    @Insert("insert into couple values(#{pid_1},#{pid_2})")
    int insertCouple(@Param("pid_1") int pid_1,
                     @Param("pid_2") int pid_2);

    @Insert("insert into child values(#{pid},#{children})")
    int insertChild(@Param("pid") int pid,
                     @Param("children") int children);


    @Update("update people set ideCard=#{ideCard},name=#{name},sex=#{sex}," +
            "telph=#{telph},naPla1=#{naPla1},naPla2=#{naPla2},adres1=#{adres1}," +
            "adres2=#{adres2},email=#{email},isMarried=#{isMarried},isArmy=#{isArmy}," +
            "eduBack=#{eduBack},health=#{health},work=#{work},isCrim=#{isCrim} where pid=#{pid}")
    int updatePeopleById(People people);

    @Update("update people set pid=#{pid},name=#{name},sex=#{sex}," +
            "telph=#{telph},naPla1=#{naPla1},naPla2=#{naPla2},adres1=#{adres1}," +
            "adres2=#{adres2},email=#{email},isMarried=#{isMarried},isArmy=#{isArmy}," +
            "health=#{health},work=#{work},isCrim=#{isCrim} where ideCard=#{ideCard}")
    int updatePeopleByIdeCard(People people);

    @Update("update people set sex=#{sex} where ideCard=#{ideCard}")
    int updatePeople_Sex_ByIdeCard(String ideCard,String sex);

    @Update("update people set name=#{name} where ideCard=#{ideCard}")
    int updatePeople_Name_ByIdeCard(String ideCard,String name);

    @Update("update people set telph=#{telph} where ideCard=#{ideCard}")
    int updatePeople_Telph_ByIdeCard(String ideCard,String telph);

    @Update("update people set napla1=#{napla1} where ideCard=#{ideCard}")
    int updatePeople_Napla1_ByIdeCard(String ideCard,String napla1);

    @Update("update people set napla2=#{napla2} where ideCard=#{ideCard}")
    int updatePeople_Napla2_ByIdeCard(String ideCard,String napla2);

    @Update("update people set adres1=#{adres1} where ideCard=#{ideCard}")
    int updatePeople_Adres1_ByIdeCard(String ideCard,String adres1);

    @Update("update people set adres2=#{adres2} where ideCard=#{ideCard}")
    int updatePeople_Adres2_ByIdeCard(String ideCard,String adres2);

    @Update("update people set email=#{email} where ideCard=#{ideCard}")
    int updatePeople_Email_ByIdeCard(String ideCard,String email);

    @Update("update people set eduBack=#{eduBack} where ideCard=#{ideCard}")
    int updatePeople_EduBack_ByIdeCard(String ideCard,int eduBack);

    @Update("update people set isMarried=#{isMarried} where ideCard=#{ideCard}")
    int updatePeople_IsMarried_ByIdeCard(String ideCard,int isMarried);

    @Update("update people set isArmy=#{isArmy} where ideCard=#{ideCard}")
    int updatePeople_IsArmy_ByIdeCard(String ideCard,int isArmy);

    @Update("update people set isCrim=#{isCrim} where ideCard=#{ideCard}")
    int updatePeople_IsCrim_ByIdeCard(People people);

    @Update("update couple set pid_2=#{pid_2} where pid_1=#{pid_1}")
    int updateCouple(int pid_1,int pid_2);

    @Update("update child set children=#{new_children} where pid=#{pid} and children=#{old_children}")
    int updateChild(@Param("pid")      int pid,
                    @Param("old_children") int old_children,
                    @Param("new_children") int new_children
    );

    @Delete("delete from people where pid=#{pid}")
    int deletePeopleById(int pid);

    @Delete("delete from couple where pid_1=#{pid} or pid_2=#{pid}")
    int deleteCouple(int pid);
}
