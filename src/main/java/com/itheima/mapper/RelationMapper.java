package com.itheima.mapper;

import com.itheima.domain.People;
import com.itheima.domain.Relation;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface RelationMapper {
    // 根据pid查找对应Relation
    @Select("select * from relation where pid=#{pid}")
    List<Relation> selectRelationById(int pid);

    @Select("select * from relation where pid=#{pid} and rel_pid= #{rel_pid}")
    Relation selectRelationByIdAndRel_pid(@Param("pid") int pid,@Param("rel_pid") int rel_pid);

    // 插入一个关系
    @Insert("insert into relation values(#{pid},#{rel_pid},#{relation})")
    int insertRelation(Relation relation);

    // 关系人
    @Update("update relation set rel_pid=#{new_pid} where pid=#{pid} and rel_pid=#{old_pid}")
    int updateRelation_Rel_pid_ById(@Param("pid")     int pid,
                                    @Param("old_pid") int old_pid,
                                    @Param("new_pid") int new_pid);

    @Update("update relation set relation=#{relation} where pid=#{pid} and rel_pid=#{rel_pid}")
    int updateRelation_Relation_ById(Relation relation);

    @Delete("delete from relation where pid=#{pid}")
    int deleteRelationById(int pid);

    @Delete("delete from relation where pid=#{pid} and rel_pid = #{rel_pid}")
    int deleteRelationByIdAndRel_pid(@Param("pid") int pid,
                                     @Param("rel_pid") int rel_pid);
}
