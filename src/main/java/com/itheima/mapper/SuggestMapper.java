package com.itheima.mapper;
import com.itheima.domain.Suggest;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface SuggestMapper {
    @Select("select * from suggest where sid=#{sid}")
    Suggest selectSuggestById(int sid);

    @Select("select * from suggest ")
    List<Suggest> selectAllSuggest();

    @Insert("insert into suggest (name,ideCard,context)" +
            "values (#{name},#{ideCard},#{context})")
    int insertSuggest(Suggest suggest);

    @Update("update suggest set name=#{name},ideCard=#{ideCard}," +
            "context=#{context} where sid=#{sid}")
    int updateSuggest(Suggest suggest);

    @Update("update suggest set name=#{name} where sid=#{sid}")
    int updateSuggest_Name_ById(int sid,String name);

    @Update("update suggest set ideCard=#{ideCard} where sid=#{sid}")
    int updateSuggest_IdeCard_ById(int sid,String ideCard);

    @Update("update suggest set context=#{context} where sid=#{sid}")
    int updateSuggest_Context_ById(int sid,String context);

    @Delete("delete from suggest where sid=#{sid}")
    int deleteSuggestById(int sid);
}
