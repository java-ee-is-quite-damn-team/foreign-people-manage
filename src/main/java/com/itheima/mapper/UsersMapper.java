package com.itheima.mapper;

import com.itheima.domain.Users;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UsersMapper {
    @Select("select * from users where ideCard=#{ideCard} AND password=#{password}")
    Users login(Users user);

    @Select("select * from users")
    List<Users> selectALLUser();

    @Select("select * from users where rec=1")
    List<Users> selectALLSLY();

    @Select("select * from users where rec=0")
    List<Users> selectALLPT();

    @Select("select * from users where ideCard=#{ideCard}")
    Users selectUserByIdeCard(@Param("ideCard") String ideCard);

    @Insert("insert into users values(#{ideCard},#{password},#{rec})")
    int insertUser(Users user);

    @Update("update users set password=#{password} where ideCard=#{ideCard}")
    int updatePassword(String password,String ideCard);

    @Delete("delete from users where ideCard=#{ideCard}")
    int deleteUsersByIdeCard(String ideCard);
}
