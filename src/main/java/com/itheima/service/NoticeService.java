package com.itheima.service;

import com.itheima.domain.Notice;

import java.util.List;

public interface NoticeService {
    Notice selectNoticeById(int nid);
    List<Notice> selectAllNotice();
    int insertNotice(Notice notice);
    int updateNotice(Notice notice);
    int updateNotice_title_ById(int nid,String title);
    int updateNotice_workPlace_ById(int nid,String workPlace);
    int updateNotice_time_ById(int nid,String time);
    int updateNotice_adrs_ById(int nid,String adrs);
    int updateNotice_type_ById(int nid,String type);
    int updateNotice_context_ById(int nid,String context);
    int deleteNotice(int nid);

}
