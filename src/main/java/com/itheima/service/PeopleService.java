package com.itheima.service;

import com.itheima.domain.People;
import java.util.List;

public interface PeopleService {
    People selectPeopleById(int pid);   // 根据id找人
    People selectPeopleByIdeCard(String ideCard); // 根据ideCard找人
    List<People> selectAllPeople(); // 找所有人
    int selectPeopleMate(int pid);   // 根据id找他(她)的伴侣
    List<People> selectPeopleChild(int pid);    // 根据id找他的孩子
    // (从数据库中获取的数据是String类型,通过分割,得出List类型)
    List<People> selectCrim();
    int insertPeople(People people);
    int insertCouple(int pid_1,int pid_2);
    int insertChild(int pid,int children);

    int updatePeopleById(People people);
    int updatePeopleByIdeCard(People people);
    int updatePeople_Sex_ByIdeCard(String ideCard,String sex);
    int updatePeople_Name_ByIdeCard(String ideCard,String name);
    int updatePeople_Telph_ByIdeCard(String ideCard,String telph);
    int updatePeople_Napla1_ByIdeCard(String ideCard,String napla1);
    int updatePeople_Napla2_ByIdeCard(String ideCard,String napla2);
    int updatePeople_Adres1_ByIdeCard(String ideCard,String adres1);
    int updatePeople_Adres2_ByIdeCard(String ideCard,String adres2);
    int updatePeople_Email_ByIdeCard(String ideCard,String email);
    int updatePeople_EduBack_ByIdeCard(String ideCard,int eduBack);
    int updatePeople_IsMarried_ByIdeCard(String ideCard,int isMarried);
    int updatePeople_IsArmy_ByIdeCard(String ideCard,int isArmy);
    int updatePeople_IsCrim_ByIdeCard(String ideCard,int isCrim);
    int updateCouple(int pid_1,int pid_2);
    int updateChild(int pid,int old_child,int new_child);
    int deletePeopleById(int pid);
    int deleteCouple(int pid);
}
