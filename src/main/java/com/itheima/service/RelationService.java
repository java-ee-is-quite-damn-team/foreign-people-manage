package com.itheima.service;

import com.itheima.domain.Relation;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface RelationService {
    // 根据pid查找对应Relation
    List<Relation> selectRelationById(int pid);

    Relation selectRelationByIdAndRel_pid(@Param("pid") int pid,@Param("rel_pid") int rel_pid);

    // 插入一个关系
    int insertRelation(Relation relation);

    // 关系人
    int updateRelation_Rel_pid_ById(int pid, int old_pid, int new_pid);
    int updateRelation_Relation_ById(Relation relation);

    int deleteRelationById(int pid);

    int deleteRelationByIdAndRel_pid(int pid,int rel_pid);
}
