package com.itheima.service;

import com.itheima.domain.Suggest;

import java.util.List;

public interface SuggestService {
    Suggest selectSuggestById(int sid);
    List<Suggest> selectAllSuggest();
    int insertSuggest(Suggest suggest);
    int updateSuggest(Suggest suggest);
    int updateSuggest_Name_ById(int sid,String name);
    int updateSuggest_IdeCard_ById(int sid,String ideCard);
    int updateSuggest_Context_ById(int sid,String context);
    int deleteSuggestById(int sid);
}
