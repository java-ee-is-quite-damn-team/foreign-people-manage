package com.itheima.service;

import com.itheima.domain.Users;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UsersService {
    Users login(Users user);
    List<Users> selectALLSLY();
    List<Users> selectALLPT();
    List<Users> selectALLUser();
    Users selectUserByIdeCard(String ideCard);
    int insertUser(Users user);
    int updatePassword(String password,String ideCard);
    int deleteUsersByIdeCard(String ideCard);
}
