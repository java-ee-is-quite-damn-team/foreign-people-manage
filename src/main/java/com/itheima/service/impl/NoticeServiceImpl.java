package com.itheima.service.impl;

import com.itheima.domain.Notice;
import com.itheima.mapper.NoticeMapper;
import com.itheima.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoticeServiceImpl implements NoticeService {
    @Autowired
    private NoticeMapper noticeMapper;
    @Override
    public Notice selectNoticeById(int nid) {
        return null;
    }

    @Override
    public List<Notice> selectAllNotice() {
        return noticeMapper.selectAllNotice();
    }

    @Override
    public int insertNotice(Notice notice) {
        int i = noticeMapper.insertNotice(notice);
        if(i != 0) System.out.println("添加成功!");
        else System.out.println("添加失败!");
        return i;
    }

    @Override
    public int updateNotice(Notice notice) {
        int i = noticeMapper.updateNotice(notice);
        if(i != 0) System.out.println("updateNotice成功!");
        else System.out.println("updateNotice失败!");
        return i;
    }

    @Override
    public int updateNotice_title_ById(int nid, String title) {
        return 0;
    }

    @Override
    public int updateNotice_workPlace_ById(int nid, String workPlace) {
        return 0;
    }

    @Override
    public int updateNotice_time_ById(int nid, String time) {
        return 0;
    }

    @Override
    public int updateNotice_adrs_ById(int nid, String adrs) {
        return 0;
    }

    @Override
    public int updateNotice_type_ById(int nid, String type) {
        return 0;
    }

    @Override
    public int updateNotice_context_ById(int nid, String context) {
        return 0;
    }

    @Override
    public int deleteNotice(int nid){
        int i = noticeMapper.deleteNotice(nid);
        if(i != 0) System.out.println("删除成功!");
        else System.out.println("删除失败!");
        return i;
    }
}
