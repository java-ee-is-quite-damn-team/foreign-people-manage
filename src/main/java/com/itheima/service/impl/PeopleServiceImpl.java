package com.itheima.service.impl;

import com.itheima.domain.People;
import com.itheima.mapper.PeopleMapper;
import com.itheima.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PeopleServiceImpl implements PeopleService {
    @Autowired
    private PeopleMapper peopleMapper;

    @Override
    public People selectPeopleById(int pid) {
        return peopleMapper.selectPeopleById(pid);
    }

    @Override
    public People selectPeopleByIdeCard(String ideCard) {
        People p = peopleMapper.selectPeopleByIdeCard(ideCard);
        if(p != null) System.out.println("selectPeopleByIdeCard成功");
        else System.out.println("selectPeopleByIdeCard没有找到");
        return p;
    }

    @Override
    public List<People> selectAllPeople() {
        return peopleMapper.selectAllPeople();
    }

    @Override
    public int selectPeopleMate(int pid) {
        Object res = peopleMapper.selectPeopleMate(pid);
        if (res != null) return (int) res;
        else return -1;
    }

    /**
     * 根据父母id查找孩子
     *
     * @param pid 父母id
     * @return
     */
    @Override
    public List<People> selectPeopleChild(int pid) {
        // 获取表示孩子的String
        List<Integer> children = peopleMapper.selectPeopleChild(pid);
        // 判断是否找到
        if (children != null) {
            List<People> res = new ArrayList<>();

            // 逐个查找并添加进res中
            for (Integer c : children) {
                People kid = peopleMapper.selectPeopleById(c);
                if (kid != null) res.add(kid);
            }
            if (res.size() != 0) return res;
            else return null;
        }
        return null;
    }

    @Override
    public List<People> selectCrim() {
        return peopleMapper.selectCrim();
    }

    @Override
    public int insertPeople(People people) {
        System.out.println("insertPeople: " + people);
        int i = peopleMapper.insertPeople(people);
        if(i > 0) System.out.println("insertPeople success");
        else System.out.println("insertPeople error");
        return i;
    }

    @Override
    public int insertCouple(int pid_1, int pid_2) {
        int i = peopleMapper.insertCouple(pid_1, pid_2);
        if (i != 0) System.out.println("添加Couple成功!");
        else System.out.println("添加Couple失败!");
        return i;
    }

    @Override
    public int insertChild(int pid, int children) {
        int i = peopleMapper.insertChild(pid, children);
        if (i != 0) System.out.println("insertChild成功!");
        else System.out.println("insertChild失败!");
        return i;
    }

    @Override
    public int updatePeopleById(People people) {
        int i = peopleMapper.updatePeopleById(people);
        if (i != 0) System.out.println("更改成功!");
        else System.out.println("更改失败!");
        return i;
    }

    @Override
    public int updatePeopleByIdeCard(People people) {
        return 0;
    }

    @Override
    public int updatePeople_Sex_ByIdeCard(String ideCard, String sex) {
        return 0;
    }

    @Override
    public int updatePeople_Name_ByIdeCard(String ideCard, String name) {
        return 0;
    }

    @Override
    public int updatePeople_Telph_ByIdeCard(String ideCard, String telph) {
        return 0;
    }

    @Override
    public int updatePeople_Napla1_ByIdeCard(String ideCard, String napla1) {
        return 0;
    }

    @Override
    public int updatePeople_Napla2_ByIdeCard(String ideCard, String napla2) {
        return 0;
    }

    @Override
    public int updatePeople_Adres1_ByIdeCard(String ideCard, String adres1) {
        return 0;
    }

    @Override
    public int updatePeople_Adres2_ByIdeCard(String ideCard, String adres2) {
        return 0;
    }

    @Override
    public int updatePeople_Email_ByIdeCard(String ideCard, String email) {
        return 0;
    }

    @Override
    public int updatePeople_EduBack_ByIdeCard(String ideCard, int eduBack) {
        return 0;
    }

    @Override
    public int updatePeople_IsMarried_ByIdeCard(String ideCard, int isMarried) {
        return 0;
    }

    @Override
    public int updatePeople_IsArmy_ByIdeCard(String ideCard, int isArmy) {
        return 0;
    }

    @Override
    public int updatePeople_IsCrim_ByIdeCard(String ideCard, int isCrim) {
        People people = new People();
        people.setIdeCard(ideCard);
        people.setIsCrim(isCrim);
        int i = peopleMapper.updatePeople_IsCrim_ByIdeCard(people);
        if (i != 0) System.out.println("更改成功!");
        else System.out.println("更改失败!");
        return i;
    }

    @Override
    public int updateCouple(int pid_1, int pid_2) {
        return 0;
    }

    @Override
    public int updateChild(int pid, int old_child, int new_child) {
        int i = peopleMapper.updateChild(pid, old_child, new_child);
        if (i != 0) System.out.println("updateChild成功!");
        else System.out.println("updateChild失败!");
        return i;
    }

    @Override
    public int deletePeopleById(int pid) {
        int i = peopleMapper.deletePeopleById(pid);
        if (i != 0) System.out.println("删除成功!");
        else System.out.println("删除失败!");
        return i;
    }

    @Override
    public int deleteCouple(int pid) {
        int i = peopleMapper.deleteCouple(pid);
        if (i != 0) System.out.println("删除couple成功!");
        else System.out.println("删除couple失败!");
        return i;
    }
}
