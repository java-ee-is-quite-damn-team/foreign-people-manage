package com.itheima.service.impl;

import com.itheima.domain.Relation;
import com.itheima.mapper.RelationMapper;
import com.itheima.service.RelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RelationServiceImpl implements RelationService {
    @Autowired
    private RelationMapper relationMapper;

    @Override
    public List<Relation> selectRelationById(int pid) {
        return relationMapper.selectRelationById(pid);
    }

    @Override
    public Relation selectRelationByIdAndRel_pid(int pid, int rel_pid) {
        Relation rel = relationMapper.selectRelationByIdAndRel_pid(pid,rel_pid);
        if(rel == null) System.out.println("selectRelationByIdAndRel_pid 找到了");
        else System.out.println("selectRelationByIdAndRel_pid 没找到");
        return rel;
    }

    @Override
    public int insertRelation(Relation relation) {
        int i = relationMapper.insertRelation(relation);
        if (i != 0) System.out.println("添加Couple成功!");
        else System.out.println("添加Couple失败!");
        return i;
    }

    @Override
    public int updateRelation_Rel_pid_ById(int pid, int old_pid, int new_pid) {
        int i = relationMapper.updateRelation_Rel_pid_ById(pid,old_pid,new_pid);
        if (i != 0) System.out.println("添加Couple成功!");
        else System.out.println("添加Couple失败!");
        return i;
    }

    @Override
    public int updateRelation_Relation_ById(Relation relation) {
        int i = relationMapper.updateRelation_Relation_ById(relation);
        if (i != 0) System.out.println("添加Couple成功!");
        else System.out.println("添加Couple失败!");
        return i;
    }

    @Override
    public int deleteRelationById(int pid) {
        int i = relationMapper.deleteRelationById(pid);
        if (i != 0) System.out.println("添加Couple成功!");
        else System.out.println("添加Couple失败!");
        return i;
    }

    @Override
    public int deleteRelationByIdAndRel_pid(int pid, int rel_pid) {
        int i = relationMapper.deleteRelationByIdAndRel_pid(pid,rel_pid);
        if (i != 0) System.out.println("添加Couple成功!");
        else System.out.println("添加Couple失败!");
        return i;
    }
}
