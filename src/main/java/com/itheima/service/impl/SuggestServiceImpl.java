package com.itheima.service.impl;

import com.itheima.domain.Suggest;
import com.itheima.mapper.SuggestMapper;
import com.itheima.service.SuggestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class SuggestServiceImpl implements SuggestService {
    @Autowired
    private SuggestMapper suggestMapper;
    @Override
    public Suggest selectSuggestById(int sid) {
        return null;
    }

    @Override
    public List<Suggest> selectAllSuggest() {
        return suggestMapper.selectAllSuggest();
    }

    @Override
    public int insertSuggest(Suggest suggest) {
        int i = suggestMapper.insertSuggest(suggest);
        if(i != 0) System.out.println("添加留言成功!");
        else System.out.println("添加留言失败!");
        return i;
    }

    @Override
    public int updateSuggest(Suggest suggest) {
        return 0;
    }

    @Override
    public int updateSuggest_Name_ById(int sid, String name) {
        return 0;
    }

    @Override
    public int updateSuggest_IdeCard_ById(int sid, String ideCard) {
        return 0;
    }

    @Override
    public int updateSuggest_Context_ById(int sid, String context) {
        return 0;
    }

    @Override
    public int deleteSuggestById(int sid) {
        return 0;
    }
}
