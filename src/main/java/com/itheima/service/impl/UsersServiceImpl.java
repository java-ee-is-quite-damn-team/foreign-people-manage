package com.itheima.service.impl;

import com.itheima.domain.Users;
import com.itheima.mapper.UsersMapper;
import com.itheima.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersServiceImpl implements UsersService {
    //注入usersMapper
    @Autowired
    private UsersMapper usersMapper;

    //通过Users的用户账号和用户密码查询用户信息
    @Override
    public Users login(Users user) {
        return usersMapper.login(user);
    }

    @Override
    public List<Users> selectALLSLY() {
        return usersMapper.selectALLSLY();
    }

    @Override
    public List<Users> selectALLPT() {
        return usersMapper.selectALLPT();
    }

    @Override
    public List<Users> selectALLUser() {
        return usersMapper.selectALLUser();
    }

    @Override
    public Users selectUserByIdeCard(String ideCard) {
        return usersMapper.selectUserByIdeCard(ideCard);
    }

    @Override
    public int insertUser(Users user) {
        System.out.println("inserUser: " + user);
        int i = usersMapper.insertUser(user);
        if(i > 0) System.out.println("insertUser success");
        else System.out.println("insertUser error");
        return i;
    }

    @Override
    public int updatePassword(String password, String ideCard) {
        return usersMapper.updatePassword(password, ideCard);
    }

    @Override
    public int deleteUsersByIdeCard(String ideCard) {
        int i = usersMapper.deleteUsersByIdeCard(ideCard);
        if (i != 0) System.out.println("删除成功!");
        else System.out.println("删除失败!");
        return i;
    }
}
