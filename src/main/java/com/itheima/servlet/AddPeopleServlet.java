//package com.itheima.servlet;
//
//import com.itheima.domain.AddPeopleDAO;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
//@WebServlet("/AddPeopleServlet")
//public class AddPeopleServlet extends HttpServlet {
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        request.setCharacterEncoding("UTF-8");
//        String name = request.getParameter("name");
//        int sex = Integer.parseInt(request.getParameter("sex"));
//        String tel = request.getParameter("tel");
//        String rel = request.getParameter("rel");
//        String card = request.getParameter("card");
//        String place = request.getParameter("place") + " " + request.getParameter("placeCity");
//        String nowPlace = request.getParameter("nowPlace") + " " + request.getParameter("nowPlaceCity");
//
//        Addpeople person = new Addpeople();
//        person.setName(name);
//        person.setSex(sex);
//        person.setTel(tel);
//        person.setRel(rel);
//        person.setCard(card);
//        person.setPlace(place);
//        person.setNowplace(nowPlace);
//
//        AddPeopleDAO dao = new AddPeopleDAO();
//        dao.addPerson(person);
//
//        response.sendRedirect(request.getContextPath() + "/carryingPeopleServlet");
//    }
//}
