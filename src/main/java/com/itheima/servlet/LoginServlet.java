//package com.itheima.servlet;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.itheima.domain.Users;
//import com.itheima.service.UserService;
//// import org.apache.commons.beanutils.BeanUtils;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * 注册servlet
// */
//@WebServlet("/login")
//public class LoginServlet extends HttpServlet{
//    private UserService userService = new UserService();
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        req.setCharacterEncoding("utf-8");
//        resp.setContentType("application/json;charset=utf-8");
//        Map<String, String[]> map = req.getParameterMap();
//        Users users = new Users();
//        HashMap hashMap = new HashMap<String,Boolean>();
//        try {
//            // BeanUtils.populate(users,map);
//            Users loginUsers = userService.login(users);
//            if(loginUsers ==null){
//                //失败
//                hashMap.put("flag",false);
//            }else{
//                //成功
//                hashMap.put("flag",true);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        //把json返回前台
//        new ObjectMapper().writeValue(resp.getWriter(),hashMap);
//    }
//}
