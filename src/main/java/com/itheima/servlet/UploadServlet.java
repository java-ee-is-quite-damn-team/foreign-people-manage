package com.itheima.servlet;


import com.itheima.domain.People;
import org.junit.Test;
import util.Copy_all;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

@WebServlet("/UploadServlet")
@MultipartConfig
public class UploadServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("UploadServlet!!!!");
        String uname = request.getParameter("uname");
        if (uname == null) {
            response.sendRedirect("/jsp/personData.jsp");
            return;
        }
        System.out.println(uname);
        Part part = request.getPart("myfile");
        if (part == null) {
            response.sendRedirect("/jsp/personData.jsp");
            return;
        }

        String fileName = part.getSubmittedFileName();

        String filePath = getServletContext().getRealPath("/userHead");
        String jdPath = "D:\\Ill\\codes\\JAVA\\test\\java-ee\\foreign-people-manage\\src\\main\\webapp\\userHead";

        File file = new File(filePath + "/" + uname + ".jpg");
        File file2 = new File(jdPath + "/" + uname + ".jpg");
        if (file.exists()) {
            file.delete();
        }
        if (file2.exists()) {
            file2.delete();
        }
        System.out.println(filePath);
        System.out.println(jdPath);
        try {
            part.write(filePath + "/" + uname + ".jpg");
        } catch (FileNotFoundException e) {
            System.out.println("创建失败");
        }

        file = new File(filePath + "/" + uname + ".jpg");
        file2 = new File(jdPath + "/" + uname + ".jpg");

        Files.copy(file.toPath(),file2.toPath());

        response.sendRedirect("/jsp/personData.jsp");
    }

    @Test
    public void demo() throws IOException {
        String filePath = "D:\\Ill\\codes\\JAVA\\test\\java-ee\\foreign-people-manage\\target\\ForeignPeopleManage-1.0-SNAPSHOT\\userHead";
        String jdPath = "D:\\Ill\\codes\\JAVA\\test\\java-ee\\foreign-people-manage\\src\\main\\webapp\\userHead";
        String uname = "352228190111140010";

        filePath += "/" + uname + ".jpg";
        jdPath += "/" + uname + ".jpg";

        File file = new File(filePath);
        File file2 = new File(jdPath);


        Files.copy(file.toPath(),file2.toPath());
        // Copy_all.FileCopy(file,file2);
    }


}

