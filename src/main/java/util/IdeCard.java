package util;

public class IdeCard {


        private static final int[] WEIGHTS = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
        private static final char[] VERIFY_CODES = {'1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'};

        public static boolean isValidChineseID(String id) {
            if (id == null || (id.length() != 18 && id.length() != 15)) {
                return false;
            }

            // 验证前17位数字的正确性
            for (int i = 0; i < id.length() - 1; i++) {
                if (!Character.isDigit(id.charAt(i))) {
                    return false;
                }
            }

            // 验证最后一位校验码的正确性
            if (id.length() == 18) {
                if (!Character.isDigit(id.charAt(17)) && id.charAt(17) != 'X') {
                    return false;
                }
                return isValidID18(id);
            } else {
                return true; // 如果是15位则不需要校验码，直接返回true
            }
        }

        private static boolean isValidID18(String id) {
            int sum = 0;
            for (int i = 0; i < id.length() - 1; i++) {
                sum += (id.charAt(i) - '0') * WEIGHTS[i];
            }

            char verifyCode = VERIFY_CODES[(sum % 11)];
            return verifyCode == id.charAt(17) || (verifyCode == 'X' && id.charAt(17) == '9');
        }

        public static void main(String[] args) {
            String id1 = "123456789012345678";
            String id2 = "350627199607172030";
            System.out.println(isValidChineseID(id1)); // 输出 false
            System.out.println(isValidChineseID(id2)); // 输出 true
        }

}
