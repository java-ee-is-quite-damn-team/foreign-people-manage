package util;
import org.mindrot.jbcrypt.BCrypt;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

public class PasswordManager {

    // 生成随机的盐值
    public static String generateSalt() throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return Base64.getEncoder().encodeToString(salt);
    }

    // 使用bcrypt对密码进行哈希，并加入盐值
    public static String hashPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt(1234));
    }

    // 验证密码是否匹配哈希值
    public static boolean verifyPassword(String rawPassword, String hashedPassword) {
        return BCrypt.checkpw(rawPassword, hashedPassword);
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        // 示例：用户注册时
        String userInputPassword = "userPassword123"; // 用户输入的密码
        // String salt = generateSalt(); // 生成盐值
        String hashedPassword = hashPassword(userInputPassword); // 哈希密码并加入盐值
        System.out.println("Hashed Password: " + hashedPassword);
        // 将hashedPassword和salt存储到数据库中

        // 示例：用户登录时
        String loginInputPassword = "userPassword123"; // 用户输入的登录密码
        // 从数据库中获取hashedPassword和salt
        // 假设这是从数据库中检索出来的
        String storedHashedPassword = hashedPassword;
        // String storedSalt = salt;

        boolean isPasswordCorrect = verifyPassword(loginInputPassword, storedHashedPassword);
        System.out.println("Is password correct? " + isPasswordCorrect);
    }
}