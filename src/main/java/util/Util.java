package util;

import java.security.NoSuchAlgorithmException;
import org.mindrot.jbcrypt.BCrypt;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {
    /**
     * 是否为合法身份证
     * @param IdeCard 身份证
     * @return 合法true,不合法false
     */
    private static final int[] WEIGHTS = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};
    private static final char[] VERIFY_CODES = {'1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'};
    private static boolean isValidID18(String id) {
        int sum = 0;
        for (int i = 0; i < id.length() - 1; i++) {
            sum += (id.charAt(i) - '0') * WEIGHTS[i];
        }

        char verifyCode = VERIFY_CODES[(sum % 11)];
        return verifyCode == id.charAt(17) || (verifyCode == 'X' && id.charAt(17) == '9');
    }
    public static boolean isLegal_IdeCard(String IdeCard){
        if (IdeCard == null || (IdeCard.length() != 18 && IdeCard.length() != 15)) {
            return false;
        }

        // 验证前17位数字的正确性
        for (int i = 0; i < IdeCard.length() - 1; i++) {
            if (!Character.isDigit(IdeCard.charAt(i))) {
                return false;
            }
        }

        // 验证最后一位校验码的正确性
        if (IdeCard.length() == 18) {
            if (!Character.isDigit(IdeCard.charAt(17)) && IdeCard.charAt(17) != 'X') {
                return false;
            }
            return isValidID18(IdeCard);
        } else {
            return true; // 如果是15位则不需要校验码，直接返回true
        }
    }

    /**
     * 是否为合法密码
     * @param password 密码
     * @return 合法true,不合法false
     * 至少有一个数字 ((?=.*[0-9]))
     * 至少有一个小写字母 ((?=.*[a-z]))
     * 至少有一个大写字母 ((?=.*[A-Z]))
     * 长度至少为8个字符 (.{8,})
     */


    public static boolean isLegal_Password(String password){
        // 定义密码强度的规则
        String passwordPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$";
        // 编译正则表达式
        Pattern pattern = Pattern.compile(passwordPattern);
        // 对输入的密码进行匹配
        Matcher matcher = pattern.matcher(password);

        // 返回匹配结果
        return matcher.matches();
    }

    // 生成随机的盐值
    public static String generateSalt() throws NoSuchAlgorithmException {
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return Base64.getEncoder().encodeToString(salt);
    }
    /**
     * 加密密码
     * @param password 密码
     * @return 加密后的密码
     */
    // 使用bcrypt对密码进行哈希，并加入盐值
    public static String hashPassword(String password,String salt){
        return BCrypt.hashpw(password, BCrypt.gensalt(Integer.parseInt(salt)));

    }

    /**
     * 验证密码
     * @param hashedPassword 加过密的密码
     * @return 正确true,不正确false
     * rawPassword：用户输入的密码
     * hashedPassword：数据库里面的密码
     */
    // 验证密码是否匹配哈希值
    public static boolean verifyPassword(String rawPassword, String hashedPassword){
        return BCrypt.checkpw(rawPassword, hashedPassword);

    }

}
