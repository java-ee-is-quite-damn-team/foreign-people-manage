﻿function setProvince(id){
    var arr = ["福建","江西","广西","浙江","湖北","山西"];
    for(var i = 0;i < arr.length;i++){
        $(id).append("<option value='"+i+"'>"+arr[i]+"</option>");
    }
}
// 根据省来得到市
function getCity(index){
    var fjian = ["福州市","厦门市","漳州市","泉州市","三明市","莆田市","南平市","龙岩市","宁德市"];
    var jxi = ["南昌市","九江市","上饶市","抚州市","宜春市","吉安市","赣州市","景德镇市","萍乡市","新余市","鹰潭市"];
    var gxi = ["南宁市","柳州市","桂林市","梧州市","北海市","防城港市","钦州市","贵港市","玉林市","百色市","贺州市","河池市","来宾市","崇左市"]
    var zjiang=["杭州市","宁波市","温州市","嘉兴市","湖州市","绍兴市","金华市","衢州市","舟山市","台州市","丽水市"];

    var taget = fjian;
    if(index == 1){taget = jxi;}
    else if(index == 2){taget = gxi;}
    else if(index == 3){taget = zjiang;}
    return taget;
}
// 将城市加到对应select标签上
function setOption(id,arr){
    for(var i = 0;i < arr.length;i++){
        $(id).append("<option value='"+i+"'>"+arr[i]+"</option>");
    }
}

function AreYouSure(){
    if (confirm('确定修改吗?')) {
        return true;
    }else return false;
}

function AreYouDel(){
    if (confirm('确定删除吗?')) {
        return true;
    }else return false;
}

function addSuccess(){
    if (confirm('确定提交吗?')) {
        return true;
    }else return false;
}
