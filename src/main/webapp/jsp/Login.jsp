<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-login.css">
    <style>
        body {
            font-size: 18px;
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background-image: url(${pageContext.request.contextPath}/image/LoginBg.jpg);
            background-size: 70%;
            background-position: 50% 50%;
            background-repeat: no-repeat;
        }

    </style>
</head>

<body>

<div class="shell">
    <h2 class="title">登录</h2>
    <form action="${pageContext.request.contextPath}/login" method="post" id="loginForm">
        <input type="text" id="userId" name="ideCard" placeholder="请输入身份证登录" onblur="return validateIdCard()" value="${param.userId}">
        <input type="password" id="UserPassword" name="password" placeholder="密码">
        <!-- <input class="passwordShow" type="image" src="../image/eye-close.png" onclick="toShowPassword()"> -->
        <input type="submit" value="登录" id="loginBtn">
    </form>
    <div class="footer">
        <div class="Remember">
            <input type="checkbox" id="rememberMe">
            <label for="rememberMe">记住我</label>
        </div>
        <a href="${pageContext.request.contextPath}/giveYourFace">人脸识别</a>
        <a href="${pageContext.request.contextPath}/jsp/Login_1.jsp" id="signUp">去注册</a>
    </div>

</div>

</body>
<script>
    // 登录和注册转换
    document.getElementById('signUp').addEventListener('click', function () {
        var title = document.querySelector('.title');
        var loginBtn = document.getElementById('loginBtn');
        var rememberMeLabel = document.querySelector('.Remember');
        var signUpBtn = document.getElementById('signUp');
        if (title.innerText === '登录') {
            title.innerText = '注册';
            loginBtn.value = '注册';
            rememberMeLabel.style.opacity = '0';
            signUpBtn.innerText = '去登录';
            document.getElementById('userId').value = '';
            document.getElementById('UserPassword').value = '';
            document.getElementById('userId').placeholder = '请输入身份证注册';
            document.getElementById('UserPassword').placeholder = '密码';
            document.getElementById('UserPassword').type = 'password';
        } else {
            title.innerText = '登录';
            loginBtn.value = '登录';
            rememberMeLabel.style.opacity = '1';
            signUpBtn.innerText = '去注册';
            document.getElementById('userId').placeholder = '请输入身份证登录'
            document.getElementById('UserPassword').placeholder = '密码';
            document.getElementById('UserPassword').type = 'password';
        }
    });
    // 显示密码
    function toShowPassword() {

        var passwordField = document.getElementById("UserPassword");
        if (passwordField.type === "password") {
            passwordField.type = "text";
        } else {
            passwordField.type = "password";
        }
    }
    //身份证验证
    function validateIdCard() {
        var idCard = document.getElementById("userId").value;
        var reg = /(^\d{15}$)|(^\d{17}(\d|X|x)$)/;
        if (!reg.test(idCard)) {
            alert("请输入正确的身份证号！");
            return false;
        }
        var provinceCode = idCard.substring(0, 2);
        var birthday = idCard.substring(6, 14);
        if (!isValidProvinceCode(provinceCode)) {
            alert("身份证号地区信息有误，请重新输入！");
            return false;
        }
        if (!isValidDate(birthday)) {
            alert("身份证号出生日期有误，请重新输入！");
            return false;
        }
        return true;
    }
    function isValidProvinceCode(code) {
        // You can add more provinces to this array
        var provinces = ["11", "12", "13", "14", "15", "21", "22", "23", "31", "32", "33", "34", "35", "36", "37", "41", "42", "43", "44", "45", "46", "50", "51", "52", "53", "54", "61", "62", "63", "64", "65"];
        return provinces.includes(code);
    }
    function isValidDate(dateString) {
        var year = parseInt(dateString.substring(0, 4));
        var month = parseInt(dateString.substring(4, 6));
        var day = parseInt(dateString.substring(6, 8));
        var date = new Date(year, month - 1, day);
        return (date.getFullYear() === year && date.getMonth() === month - 1 && date.getDate() === day);
    }
</script>

</html>