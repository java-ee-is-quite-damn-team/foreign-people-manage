<%@ page import="com.itheima.domain.Relation" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>携带人口管理</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-train.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-person-data.css">
  <style>
    body {
      background-image: url(${pageContext.request.contextPath}/image/background.png);
      background-repeat: no-repeat;
      background-attachment: fixed;
      background-size: 100%;
    }
  </style>
</head>
<body>
<%@include file="publicModel/head.jsp"%>
<div class="root">
  <div style="margin-top: 20px; margin-left: 18%;">
    当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> > <a>携带人口管理</a>
  </div>
  <p></p>

  <!-- 主体 -->
  <div class="main-container">
    <div class="main-container-title">携带人口管理</div>
    <a href="${pageContext.request.contextPath}/addRelation"><input type="button" value="添加"></a>
    <div class="content-header">
      <table class="content-table" style="width: 100%;">
        <thead>
        <tr>
          <th>姓名</th>
          <th>身份证号</th>
          <th>与本人关系</th>
          <th>管理</th>
        </tr>
        </thead>
        <tbody>

        <%
            List<Relation> carrPeople = (List<Relation>) session.getAttribute("carryPeople");
            for(int i = 0;i < carrPeople.size();i++){%>
            <c:set var="person" value="<%=carrPeople.get(i)%>"></c:set>
            <tr>
            <td>${person.name}</td>
            <td>${person.ideCard}</td>
            <td>${person.relation}</td>
            <td>
                <a href="${pageContext.request.contextPath}/updateRelation?index=<%=i%>&pid=${person.pid}"> <input type="button" value="更改"></a>
                <a href="${pageContext.request.contextPath}/deleteRelation?index=<%=i%>"> <input type="button" value="删除"></a>
            </td>
          </tr>
        <%}%>
        </tbody>
      </table>
    </div>
  </div>
</div>
<%@include file="publicModel/button.jsp"%>
</body>
</html>
