<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.itheima.domain.People" %><%--
  Created by IntelliJ IDEA.
  User: Hater
  Date: 2024/5/23
  Time: 9:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>结婚生育信息管理</title>
    <%@include file="publicModel/cssInput.jsp" %>
    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>
</head>
<body>
<%@include file="publicModel/head.jsp" %>
<div class="root">
    <div style="margin-top: 20px; margin-left: 18%;">
        当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> > 结婚生育信息管理</a>
    </div>
    <p></p>

    <!-- 主体 -->
    <div class="main-container">
        <div class="content-header">
            <div id="hiai">
                <br>
                <div class="main-container-title"> 子女信息填写</div>
                <div class="person-data" style="display: flex;justify-content: center;">
                    <form action="${pageContext.request.contextPath}/<%if((int)request.getAttribute("choose")!=-1){%>updateChild<%}else{%>addChild<%}%>" method="post">
                        <input type="hidden" name="choose" value="${choose}">
                        <div class="person-data-1">
                            <table class="content-table-mar" style="width: 100%;">
                                <tbody>
                                <tr>
                                    <td>姓&nbsp;&nbsp;名:</td>
                                    <td><input type="text" name="name"
                                               value="<c:if test="${child!=null}">${child.getName()}</c:if>"></td>
                                </tr>

                                <tr>
                                    <td>性&nbsp;&nbsp;别:</td>
                                    <td>
                                        <select style="width: 170px;text-align: center;font-size: 16px;" name="sex">
                                            <option value="0" <c:if test="${child!=null && child.getSex()==0}">selected="selected"</c:if>>女</option>
                                            <option value="1" <c:if test="${child!=null && child.getSex()==1}">selected="selected"</c:if>>男</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td>手机号:</td>
                                    <td><input type="text" name="telph"

                                               value="<c:if test="${child!=null}">${child.getTelph()}</c:if>"></td>
                                </tr>

                                <tr>
                                    <td>身份证号:</td>
                                    <td><input type="text" name="ideCard" id="userId" onblur="return validateIdCard()"
                                               value="<c:if test="${child!=null}">${child.getIdeCard()}</c:if>"></td>
                                </tr>

                                <tr>
                                    <td>籍&nbsp;&nbsp;贯:</td>
                                    <td>
                                        <select id="naPla1" style="font-size: 16px;width: 82px;" name="naPla1">
                                        </select>
                                        <select id="naPla2" style="font-size: 16px;width: 82px;" name="naPla2">
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td>现居住地址:</td>
                                    <td>
                                        <select id="adres1" style="font-size: 16px;width: 82px;" name="adres1">
                                        </select>
                                        <select id="adres2" style="font-size: 16px;width: 82px;" name="adres2">
                                        </select>
                                        <br>
                                        <!-- <input type="text" name="address3"></td> </tr> -->
                                <tr>
                                    <td>邮箱:</td>
                                    <td><input type="text" name="email"
                                               value="<c:if test="${child!=null}">${child.getEmail()}</c:if>"></td>
                                </tr>

                                <tr>
                                    <td>学历:</td>
                                    <td><select style="width: 170px;text-align: center;font-size: 16px;" name="eduBack">
                                        <option value="1"
                                                <c:if test="${  child !=null &&   child.getEduBack() == 1}">selected="selected" </c:if>>
                                            小学
                                        </option>
                                        <option value="2"
                                                <c:if test="${  child !=null &&   child.getEduBack() == 2}">selected="selected" </c:if>>
                                            初中
                                        </option>
                                        <option value="3"
                                                <c:if test="${  child !=null &&   child.getEduBack() == 3}">selected="selected" </c:if>>
                                            高中/职高
                                        </option>
                                        <option value="4"
                                                <c:if test="${  child !=null &&   child.getEduBack() == 4}">selected="selected" </c:if>>
                                            专科
                                        </option>
                                        <option value="5"
                                                <c:if test="${  child !=null &&   child.getEduBack() == 5}">selected="selected" </c:if>>
                                            本科
                                        </option>
                                        <option value="6"
                                                <c:if test="${  child !=null &&   child.getEduBack() == 6}">selected="selected" </c:if>>
                                            硕士
                                        </option>
                                        <option value="7"
                                                <c:if test="${  child !=null &&   child.getEduBack() == 7}">selected="selected" </c:if>>
                                            博士
                                        </option>
                                    </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td>婚姻状况:</td>
                                    <td><select style="width: 170px;text-align: center;font-size: 16px;"
                                                name="isMarried">

                                        <option value="0"
                                                <c:if test="${child!=null && child.getIsMarried()==0}">selected="selected"</c:if>>未婚
                                        </option>
                                        <option value="1"
                                                <c:if test="${child!=null && child.getIsMarried()==1}">selected="selected"</c:if>>已婚
                                        </option>
                                    </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td>服兵役状况:</td>
                                    <td><select style="width: 170px;text-align: center;font-size: 16px;" name="isArmy">
                                        <option value="0"
                                                <c:if test="${child!=null && child.getIsArmy()==0}">selected="selected"</c:if>>未服兵役
                                        </option>
                                        <option value="1"
                                                <c:if test="${child!=null && child.getIsArmy()==1}">selected="selected"</c:if>>已服兵役
                                        </option>
                                    </select>
                                    </td>
                                </tr>
                                <tr style="text-align: center">
                                    <td colspan="2">
                                        <input type="submit" value="修改"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<%@include file="publicModel/button.jsp" %>
<c:set var="chooseP" value="${child}"></c:set>
<script type="text/javascript">
    function mate() {
        var naPla1 = 0;
        var naPla2 = 0;
        var adres1 = 0;
        var adres2 = 0;
        <c:if test="${chooseP != null}">
        naPla1 = ${chooseP.naPla1};
        naPla2 = ${chooseP.naPla2};
        adres1 = ${chooseP.adres1};
        adres2 = ${chooseP.adres2};
        </c:if>

        setProvince("#naPla1");
        setProvince("#adres1")
        $("#naPla1").val(naPla1);
        $("#adres1").val(adres1);

        var taget = getCity(naPla1);
        setOption("#naPla2", taget);
        taget = getCity(adres1);
        setOption("#adres2", taget);
        $("#naPla2").val(naPla2);
        $("#adres2").val(adres2);

        $("#naPla1").change(function () {
            var opt = $("#naPla1").val();
            var arr = getCity(opt);
            $("#naPla2").find("option").remove();
            setOption("#naPla2", arr);
        });
        $("#adres1").change(function () {
            var opt = $("#adres1").val();
            var arr = getCity(opt);
            $("#adres2").find("option").remove();
            setOption("#adres2", arr);
        });
    }

    $(function () {
        mate()
    })

    //身份证验证
    function validateIdCard() {
        var idCard = document.getElementById("userId").value;
        var reg = /(^\d{15}$)|(^\d{17}(\d|X|x)$)/;
        if (!reg.test(idCard)) {
            alert("请输入正确的身份证号！");
            return false;
        }
        var provinceCode = idCard.substring(0, 2);
        var birthday = idCard.substring(6, 14);
        if (!isValidProvinceCode(provinceCode)) {
            alert("身份证号地区信息有误，请重新输入！");
            return false;
        }
        if (!isValidDate(birthday)) {
            alert("身份证号出生日期有误，请重新输入！");
            return false;
        }
        return true;
    }
    function isValidProvinceCode(code) {
        // You can add more provinces to this array
        var provinces = ["11", "12", "13", "14", "15", "21", "22", "23", "31", "32", "33", "34", "35", "36", "37", "41", "42", "43", "44", "45", "46", "50", "51", "52", "53", "54", "61", "62", "63", "64", "65"];
        return provinces.includes(code);
    }
    function isValidDate(dateString) {
        var year = parseInt(dateString.substring(0, 4));
        var month = parseInt(dateString.substring(4, 6));
        var day = parseInt(dateString.substring(6, 8));
        var date = new Date(year, month - 1, day);
        return (date.getFullYear() === year && date.getMonth() === month - 1 && date.getDate() === day);
    }
</script>
</body>
</html>
