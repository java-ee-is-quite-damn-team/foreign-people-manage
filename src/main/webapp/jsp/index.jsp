<%@ page import="java.util.List" %>
<%@ page import="com.itheima.domain.Notice" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>首页</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/fuwu.css">

    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;

        }
    </style>
</head>
<body>
<%@include file="publicModel/head.jsp" %>
<%
    List<Notice> notices = (List<Notice>) session.getAttribute("NOTICES");
    if(notices == null || notices.size() == 0){
        response.sendRedirect("/index/");
        return;
    }
%>
<div class="root">

    <div style="margin-top: 20px; margin-left: 18%;">
        当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> > 全部公告</a>
    </div>
    <p></p>

    <!-- 主体 -->
    <div class="main-container">

        <!-- 上半部分 业务块 -->
        <div class="menu">
            <div class="menu-filter">
                <ul class="MF">
                    <%
                        if (notices != null)
                            for (int i = 0; i < notices.size(); i++) {
                                Notice notice = notices.get(i);
                    %>
                    <a href="${pageContext.request.contextPath}/seeTheNotice?index=<%=i%>">
                        <li class="menu-item">
                            <img class="menu-icon" src="${pageContext.request.contextPath}/image/qbyw.svg" alt="">
                            <p class="menu-title"><%=notice.getTitle()%>
                            </p>
                        </li>
                    </a>
                    <%}%>
                </ul>
            </div>
        </div>

        <hr>
        <%--        <!-- 搜索框 -->--%>
        <%--        <div class="search-box" style="margin-right: 1%;">--%>
        <%--            <div class="search">--%>
        <%--                <form action="" >--%>
        <%--                    <input type="text" style="width: 100px;border-radius: 15px; justify-content: center;">--%>
        <%--                    <input type="submit" value="搜索"--%>
        <%--                           style=" background-color: #fff; color: #333;--%>
        <%--							font-size: 16px;font-weight: bold; ">--%>
        <%--                </form>--%>
        <%--            </div>--%>
        <%--        </div>--%>
        <br>
        <!-- 下半部分 -->
        <div class="content">
            <div class="content-header">
                <table class="content-table" style="width: 100%;">
                    <thead>
                    <tr>
                        <th>实施事项名称</th>
                        <th>地区</th>
                        <th>发布单位</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        for (Notice notice : notices) {
                    %>
                    <tr class="tbody-tr"
                        data-url="/PublicPlatform/front/areas/show.action?code=C2020080318093165143&amp;&amp;type=政府服务">

                        <td><%=notice.getTitle()%>
                        </td>
                        <td><%=notice.getAdrs()%>
                        </td>
                        <td><%=notice.getWorkPlace()%>
                        </td>
                    </tr>
                    <%}%>

                    </tbody>
                </table>

                <!-- 分页 -->
                <div class="fenye">
                    <div class="layui-laypage" id="layui-laypage-2">
                        <a href="javascript:;" class="layui-laypage-prev layui-disabled" data-page="0">上一页</a>
                        <span class="layui-laypage-curr">
								<em class="layui-laypage-em" style="background-color:#6BB3FF;"></em>
								<em>1</em>
								</span><a href="javascript:;" data-page="2">2</a>
                        <a href="javascript:;" data-page="3">3</a>
                        <a href="javascript:;" data-page="4">4</a>
                        <a href="javascript:;" data-page="5">5</a>
                        <a href="javascript:;" class="layui-laypage-next" data-page="2">下一页</a>
                        <span class="layui-laypage-limits"><select lay-ignore="">
										<option value="10" selected="">10 条/页</option>
										<option value="20">20 条/页</option>
										<option value="30">30 条/页</option>
										<option value="40">40 条/页</option>
										<option value="50">50 条/页</option>
										</select>
								</span>
                        <span class="layui-laypage-skip">到第<input style="text-align: center;" type="text" min="1"
                                                                    value="1" class="layui-input">页
										<button type="button" style="font-size: 12px"
                                                class="layui-laypage-btn">确定</button>
								</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="publicModel/button.jsp" %>

</body>
</html>