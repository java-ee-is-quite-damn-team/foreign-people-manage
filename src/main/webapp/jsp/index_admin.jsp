<%@ page import="java.util.List" %>
<%@ page import="com.itheima.domain.Notice" %>
<%@ page import="java.util.ArrayList" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>公告管理</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-person-data.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/bootstrap-3.4.1/css/bootstrap.css">
    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;

        }
    </style>


</head>
<body>
<%
    if(session.getAttribute("DELNOTICE") == null){
        session.setAttribute("DELNOTICE", new ArrayList<Integer>());
    }
%>
<%@include file="publicModel/head_admin.jsp" %>
<div class="root">

    <div style="margin-top: 20px; margin-left: 18%;">
        当前位置：<a href="${pageContext.request.contextPath}/jsp/index_admin.jsp"> 首页 </a> > 公告管理</a>
    </div>
    <p></p>

    <!-- 主体 -->
    <div class="main-container">
        <div>
            <div class="main-container-title" style="display: inline-block;">公告管理</div>

<%--            <!-- 搜索框 -->--%>
<%--            <div class="search-box">--%>
<%--                <div class="search">--%>
<%--                    <form action="">--%>
<%--                        <input type="text" style="width: 120px;border-radius: 15px;">--%>
<%--                        <input type="submit" value="搜索"--%>
<%--                               style=" background-color: #fff; color: #333;--%>
<%--								font-size: 16px;font-weight: bold; ">--%>
<%--                    </form>--%>
<%--                </div>--%>
<%--            </div>--%>

            <div class="content-header">
                <a href="${pageContext.request.contextPath}/jsp/informationManager-1.jsp?typeis=add">
                <span style="color: #00508b;font-size: 24px;">
                    <b>添加公告</b>
                </span>
                </a>
                <table class="content-table" style="width: 100%;">
                    <thead>
                    <tr>
                        <th >标题</th>
                        <th >发布单位</th>
                        <th >时间</th>
                        <th >地区</th>
                        <th >公告类型</th>
                        <th >管理</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%
                        List<Notice> notices = (List<Notice>) session.getAttribute("NOTICES");
                        for (int i = 0;i < notices.size();i++) {
                            Notice notice = notices.get(i);
                    %>
                    <tr id="<%=i%>">
                        <td><%=notice.getTitle()%>
                        </td>
                        <td><%=notice.getWorkPlace()%>
                        </td>
                        <td><%=notice.getTime()%>
                        </td>
                        <td><%=notice.getAdrs()%>
                        </td>
                        <td><%=notice.getType()%>
                        </td>
                        <td>
                            <a href="${pageContext.request.contextPath}/updateNoice?index=<%=i%>" onclick="return AreYouSure()">
                                <span class="btn-primary">修改</span></a>
                            <a href="${pageContext.request.contextPath}/indexAdmin?typeis=del&index=<%=i%>" onclick="return AreYouDel()">
                                <span class="btn-danger">删除</span></a>
                        </td>
                    </tr>
                    <%}%>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<%@include file="publicModel/button.jsp" %>
<script type="text/javascript">
    $(function (){
        $("input").click(function(){
            // var index = $(this).parent().parent().id;
            $(this).parent().parent().remove();
        })
    })
</script>
</body>
</html>