<%@ page import="com.itheima.domain.Notice" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>首页</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">

    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>


</head>
<body>
<div class="root">

    <%@include file="publicModel/head.jsp"%>


    <div style="margin-top: 20px; margin-left: 18%;">
        当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> > <a
            href="${pageContext.request.contextPath}/jsp/index.jsp">全部业务</a> > 公告内容
    </div>
    <p></p>
    <%
        Notice notice = (Notice) request.getAttribute("NOTICE_INFO");
    %>
    <!-- 主体 -->
    <div class="main-container">
        <!-- 公告详情 -->
        <div class="information">
            <div>
                <p>
                <h2><%=notice.getTitle()%></h2>
                </p>
            </div>
            <div><p style="font-size: 14px;">发布单位：<%=notice.getWorkPlace()%></p></div>
            <div><p style="font-size: 14px;">时间：<%=notice.getTime()%></p></div>
            <div class="informationBody"><p><%=notice.getContext()%></p></div>
        </div>
    </div>
    <%@include file="publicModel/button.jsp"%>
</div>

</body>
</html>