<%@ page import="com.itheima.domain.Notice" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>公告管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-person-data.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/bootstrap-3.4.1/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/bootstrap-datepicker-1.9.0/css/bootstrap-datepicker.css">
    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;

        }
    </style>


</head>
<body>
<%@include file="publicModel/head_admin.jsp"%>

<%
    Notice thisNotion = (Notice)request.getAttribute("thisNotion");
%>
<div class="root">
    <div style="margin-top: 20px; margin-left: 18%;">
        当前位置：<a href="${pageContext.request.contextPath}/jsp/index_admin.jsp"> 首页 </a> > <a>公告管理</a>
    </div>
    <p></p>

    <!-- 主体 -->
    <div class="main-container">
        <div class="main-container-title">公告管理</div>

        <!-- 个人资料表格 -->
        <div class="person-data">
            <form action="${pageContext.request.contextPath}/<%if (thisNotion == null){%>addNoice<%}else{%>updateNoice?index=${isUpdate}<%}%>" method="post">
                <div class="person-data-1">
                    <table border="0;">
                        <tbody>
                        <tr>
                            <td><b>标&nbsp;&nbsp;题:</b></td>
                            <td><input type="text" class="form-control" name="title" placeholder="标题" value="${thisNotion.getTitle()}"></td>
                        </tr>

                        <tr>
                            <td><b>发布单位:</b></td>
                            <td><input type="text" class="form-control" name="workPlace" placeholder="发布单位" value="${thisNotion.getWorkPlace()}"></td>
                        </tr>
                        <tr>
                            <td><b>发布时间:</b></td>
                            <td><input type="text" class="form-control" id="dt" placeholder="发布时间" name="time" value="${thisNotion.getTime()}"></td>
                        </tr>
                        <tr>
                            <td><b>地区:</b></td>
                            <td><input type="text" class="form-control" placeholder="地区" name="adrs" value="${thisNotion.getAdrs()}"></td>
                        </tr>
                        <tr>
                            <td><b>公告类型:</b></td>
                            <td><input type="text" class="form-control" placeholder="公告类型" name="type" value="${thisNotion.getType()}"></td>
                        </tr>
                        <tr>
                            <td><b>公告内容:</b></td>
                            <td><textarea class="form-control" style="height:300px;width:500px;" placeholder="公告内容" name="context">${thisNotion.getContext()}</textarea></td>
                        </tr>

                        <tr style="text-align: center">
                            <td colspan="2">
                                <input type="submit" onclick="return addSuccess()" value="保存">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>

</div>
<%@include file="publicModel/button.jsp"%>

<script>
    $(function (){
        $('#dt').datepicker({
            format: "yyyy-mm-dd",   //日期格式
            startDate: "0",         //起始时间
            language: "zh-CN",
            autoclose: true
        })
    })
</script>
</body>
</html>