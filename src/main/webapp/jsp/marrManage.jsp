<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.itheima.domain.People" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>结婚生育信息管理</title>
    <%@include file="publicModel/cssInput.jsp" %>
    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>
</head>
<body>
<%@include file="publicModel/head.jsp" %>
<div class="root">
    <div style="margin-top: 20px; margin-left: 18%;">
        当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> > 结婚生育信息管理</a>
    </div>
    <p></p>

    <!-- 主体 -->
    <div class="main-container">
        <div class="main-container-title">婚姻状况</div>
        <div class="content-header">
            <table class="content-table" style="width: 100%;">
                <thead>
                <tr>
                    <th>是否结婚</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <%
                            if (((People) session.getAttribute("USER_INFORMATION")).getIsMarried() != 0)
                                out.print("是");
                            else out.print("否");
                        %>
                    </td>
                </tr>
                </tbody>
            </table>
            <%if (((People) session.getAttribute("USER_INFORMATION")).getIsMarried() != 0) {
                People mateP = (People) session.getAttribute("USER_MARRIED");
            %>
            <br>
            <table class="content-table" style="width: 100%;">
                <tr>
                    <th>配偶姓名</th>
                    <th>配偶身份证号</th>
                    <th>管理</th>
                </tr>
                <tr>
                    <td>
                        <%if (mateP != null) {%><%=mateP.getName()%><%} else {%>请通过管理添加<%}%>
                    </td>
                    <td>
                        <%if (mateP != null) {%><%=mateP.getIdeCard()%><%} else {%>请通过管理添加<%}%>
                    </td>
                    <td><a href="${pageContext.request.contextPath}/addMate" class="btn-primary btn-sm">管理</a>
                </tr>
            </table>

            <br>

            <p></p>
            <div class="main-container-title" class="clearfix">
                子女状况
                <a href="${pageContext.request.contextPath}/addChild" style="float: right;font-size: 15px;" class="btn-primary btn-xs">添加子女</a>
            </div>

            <table class="content-table" style="width: 100%;">
                <thead>
                <tr>
                    <th>子女状况</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <%if(session.getAttribute("children")!=null){%>${children.size()}<%}else{%>0<%}%>个子女
                    </td>
                </tr>
                </tbody>
            </table>
            <br>
            <c:if test="${children != null}">
                <%int i = 0;%>
                <table class="content-table" style="width: 100%;">
                    <tr>
                        <th>子女姓名</th>
                        <th>子女身份证号</th>
                        <th>管理</th>
                    </tr>
                    <c:forEach items="${children}" var="child">
                        <tr>
                            <td>${child.getName()}</td>
                            <td>${child.getIdeCard()}</td>
                            <td>
                                <a href="${pageContext.request.contextPath}/updateChild?choose=<%=i%>" class="btn-primary btn-sm">管理</a>
                            </td>
                        </tr>
                        <%i++;%>
                    </c:forEach>
                </table>
            </c:if>
            <%}%>
        </div>
    </div>
</div>

<%@include file="publicModel/button.jsp" %>
<c:set var="chooseP" value="${USER_MARRIED}"></c:set>
<script type="text/javascript">
</script>
</body>
</html>