<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.itheima.domain.People" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>结婚生育信息管理</title>
    <%@include file="publicModel/cssInput.jsp" %>
    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>
</head>
<body>
<%@include file="publicModel/head.jsp" %>
<%
    People mateP = (People) session.getAttribute("USER_MARRIED");
%>
<div class="root">
    <div style="margin-top: 20px; margin-left: 18%;">
        当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> >
        <a href="${pageContext.request.contextPath}/jsp/marrManage.jsp">结婚生育信息管理</a>
    </div>
    <p></p>

    <!-- 主体 -->
    <div class="main-container">
        <div class="content-header">
            <div id="hiai">
                <br>
                <div class="main-container-title"> 配偶信息填写</div>
                <div class="person-data" style="display: flex;justify-content: center;">
                    <form action="${pageContext.request.contextPath}/addMate" method="post">
                        <div class="person-data-1">
                            <table class="content-table-mar" style="width: 100%;">
                                <tbody>
                                <tr>
                                    <td>姓&nbsp;&nbsp;名:</td>
                                    <td><input type="text" name="name"
                                               value="<%if(mateP!=null){%>${USER_MARRIED.getName()}<%}%>"></td>
                                </tr>

                                <tr>
                                    <td>性&nbsp;&nbsp;别:</td>
                                    <td>
                                        <select style="width: 170px;text-align: center;font-size: 16px;" name="sex">
                                            <option value="0"
                                                    <%if(mateP!=null && mateP.getSex()==0){%>selected="selected"<%}%>>女
                                            </option>
                                            <option value="1"
                                                    <%if(mateP!=null && mateP.getSex()==1){%>selected="selected"<%}%>>男
                                            </option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td>手机号:</td>
                                    <td><input type="text" name="telph"
                                               value="<%if(mateP!=null){%>${USER_MARRIED.getTelph()}<%}%>"></td>
                                </tr>

                                <tr>
                                    <td>身份证号:</td>
                                    <td><input type="text" id="userId" name="ideCard" onblur="return validateIdCard()"
                                               value="<%if(mateP!=null){%>${USER_MARRIED.getIdeCard()}<%}%>"></td>
                                </tr>

                                <tr>
                                    <td>籍&nbsp;&nbsp;贯:</td>
                                    <td>
                                        <select id="naPla1" style="font-size: 16px;width: 82px;" name="naPla1">
                                        </select>
                                        <select id="naPla2" style="font-size: 16px;width: 82px;" name="naPla2">
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td>现居住地址:</td>
                                    <td>
                                        <select id="adres1" style="font-size: 16px;width: 82px;" name="adres1">
                                        </select>
                                        <select id="adres2" style="font-size: 16px;width: 82px;" name="adres2">
                                        </select>
                                        <br>
                                        <!-- <input type="text" name="address3"></td> </tr> -->
                                <tr>
                                    <td>邮箱:</td>
                                    <td><input type="text" name="email"
                                               value="<%if(mateP!=null){%>${USER_MARRIED.getEmail()}<%}%>"></td>
                                </tr>

                                <tr>
                                    <td>学历:</td>
                                    <td><select style="width: 170px;text-align: center;font-size: 16px;" name="eduBack">
                                        <option value="1"
                                                <c:if test="${USER_MARRIED !=null && USER_MARRIED.getEduBack() == 1}">selected="selected" </c:if>>
                                            小学
                                        </option>
                                        <option value="2"
                                                <c:if test="${USER_MARRIED !=null && USER_MARRIED.getEduBack() == 2}">selected="selected" </c:if>>
                                            初中
                                        </option>
                                        <option value="3"
                                                <c:if test="${USER_MARRIED !=null && USER_MARRIED.getEduBack() == 3}">selected="selected" </c:if>>
                                            高中/职高
                                        </option>
                                        <option value="4"
                                                <c:if test="${USER_MARRIED !=null && USER_MARRIED.getEduBack() == 4}">selected="selected" </c:if>>
                                            专科
                                        </option>
                                        <option value="5"
                                                <c:if test="${USER_MARRIED !=null && USER_MARRIED.getEduBack() == 5}">selected="selected" </c:if>>
                                            本科
                                        </option>
                                        <option value="6"
                                                <c:if test="${USER_MARRIED !=null && USER_MARRIED.getEduBack() == 6}">selected="selected" </c:if>>
                                            硕士
                                        </option>
                                        <option value="7"
                                                <c:if test="${USER_MARRIED !=null && USER_MARRIED.getEduBack() == 7}">selected="selected" </c:if>>
                                            博士
                                        </option>
                                    </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td>婚姻状况:</td>
                                    <td><select style="width: 170px;text-align: center;font-size: 16px;"
                                                name="isMarried">
                                        <option value="0"
                                                <%if(mateP!=null && mateP.getIsMarried()==0){%>selected="selected"<%}%>>未婚
                                        </option>
                                        <option value="1"
                                                <%if(mateP!=null && mateP.getIsMarried()==1){%>selected="selected"<%}%>>已婚
                                        </option>
                                    </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td>服兵役状况:</td>
                                    <td><select style="width: 170px;text-align: center;font-size: 16px;" name="isArmy">
                                        <option value="0"
                                                <%if(mateP!=null && mateP.getIsArmy()==0){%>selected="selected"<%}%>>未服兵役
                                        </option>
                                        <option value="1"
                                                <%if(mateP!=null && mateP.getIsArmy()==1){%>selected="selected"<%}%>>已服兵役
                                        </option>
                                    </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>头像:</td>
                                    <td><input type="file" name="userHead"></td>
                                </tr>
                                <tr style="text-align: center">
                                    <td colspan="2">
                                        <input type="submit" value="提交"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<%@include file="publicModel/button.jsp" %>

<c:set var="chooseP" value="${USER_MARRIED}"></c:set>
<script type="text/javascript">
    function mate() {
        var naPla1 = 0;
        var naPla2 = 0;
        var adres1 = 0;
        var adres2 = 0;
        <c:if test="${chooseP != null}">
        naPla1 = ${chooseP.getNaPla1()};
        naPla2 = ${chooseP.getNaPla2()};
        adres1 = ${chooseP.getAdres1()};
        adres2 = ${chooseP.getAdres2()};
        </c:if>

        setProvince("#naPla1");
        setProvince("#adres1")
        $("#naPla1").val(naPla1);
        $("#adres1").val(adres1);

        var taget = getCity(naPla1);
        setOption("#naPla2", taget);
        taget = getCity(adres1);
        setOption("#adres2", taget);
        $("#naPla2").val(naPla2);
        $("#adres2").val(adres2);

        $("#naPla1").change(function () {
            var opt = $("#naPla1").val();
            var arr = getCity(opt);
            $("#naPla2").find("option").remove();
            setOption("#naPla2", arr);
        });
        $("#adres1").change(function () {
            var opt = $("#adres1").val();
            var arr = getCity(opt);
            $("#adres2").find("option").remove();
            setOption("#adres2", arr);
        });
    }

    $(function () {
        mate()
    })

    //身份证验证
    function validateIdCard() {
        var idCard = document.getElementById("userId").value;
        var reg = /(^\d{15}$)|(^\d{17}(\d|X|x)$)/;
        if (!reg.test(idCard)) {
            alert("请输入正确的身份证号！");
            return false;
        }
        var provinceCode = idCard.substring(0, 2);
        var birthday = idCard.substring(6, 14);
        if (!isValidProvinceCode(provinceCode)) {
            alert("身份证号地区信息有误，请重新输入！");
            return false;
        }
        if (!isValidDate(birthday)) {
            alert("身份证号出生日期有误，请重新输入！");
            return false;
        }
        return true;
    }
    function isValidProvinceCode(code) {
        // You can add more provinces to this array
        var provinces = ["11", "12", "13", "14", "15", "21", "22", "23", "31", "32", "33", "34", "35", "36", "37", "41", "42", "43", "44", "45", "46", "50", "51", "52", "53", "54", "61", "62", "63", "64", "65"];
        return provinces.includes(code);
    }
    function isValidDate(dateString) {
        var year = parseInt(dateString.substring(0, 4));
        var month = parseInt(dateString.substring(4, 6));
        var day = parseInt(dateString.substring(6, 8));
        var date = new Date(year, month - 1, day);
        return (date.getFullYear() === year && date.getMonth() === month - 1 && date.getDate() === day);
    }
</script>


</body>
</html>
