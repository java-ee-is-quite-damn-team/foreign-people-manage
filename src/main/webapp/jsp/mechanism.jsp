<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>社会组织</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-train.css">

    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>

</head>
<body>
<%@include file="publicModel/head.jsp" %>
<div style="margin-top: 20px; margin-left: 18%;">
    当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> > 社会组织</a>
</div>
<br>


<!-- 主体 -->
<div class="main-container" style="height: 700px">
    <div class="main-container-title"> 公益救助项目</div>

    <div class="pp-item-1">
        <div class="pp-item-2">
            <img class="pp-item-img" src="${pageContext.request.contextPath}/image/mechanism-1.png" alt="">
            <p class="pp-item-text">医疗</p>
        </div>
        <div class="pp-item-2">
            <img class="pp-item-img" src="${pageContext.request.contextPath}/image/mechanism-2.png" alt="">
            <p class="pp-item-text">生育</p>
        </div>
        <div class="pp-item-2">
            <img class="pp-item-img" src="${pageContext.request.contextPath}/image/mechanism-3.png" alt="">
            <p class="pp-item-text">教育</p>
        </div>
        <div class="pp-item-2">
            <img class="pp-item-img" src="${pageContext.request.contextPath}/image/mechanism-4.png" alt="">
            <p class="pp-item-text">住房</p>
        </div>
        <div class="pp-item-2">
            <img class="pp-item-img" src="${pageContext.request.contextPath}/image/mechanism-5.png" alt="">
            <p class="pp-item-text">失业救助</p>
        </div>
    </div>
</div>

<%@include file="publicModel/button.jsp" %>
</body>
</html>