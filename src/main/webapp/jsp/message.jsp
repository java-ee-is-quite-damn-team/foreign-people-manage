<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人中心</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-person-data.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/bootstrap-3.4.1/css/bootstrap.css">
    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>

</head>
<body>
<%@include file="publicModel/head.jsp" %>

<div style="margin-top: 20px; margin-left: 18%;">
    当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> > 留言</a>
</div>
<p></p>


<!-- 主体 -->
<div class="main-container">
    <div class="main-container-title"> 实时通信-与管理员</div>
    <%@include file="publicModel/chat.jsp"%>

    <div class="main-container-title"> 留言</div>

    <!-- 个人资料表格 -->
    <div class="person-data">
        <form action="${pageContext.request.contextPath}/addSuggest" method="">
            <div class="person-data-1">
                <table border="0;">
                    <tbody>
                    <tr>
                        <td><b>留&nbsp;&nbsp;言:</b></td>
                        <td>
                            <textarea name="context" style="height: 300px;"></textarea>
                        </td>
                    </tr>
                    <tr style="text-align: center">
                        <td colspan="2">
                            <input type="submit" onclick="return addSuccess()" value="提交"></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>

<%@include file="publicModel/button.jsp" %>

</body>
</html>