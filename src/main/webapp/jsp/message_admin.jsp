<%@ page import="com.itheima.domain.Suggest" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>留言管理</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-person-data.css">

    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>

</head>
<body>
<%
    List<Suggest> suggests = (List<Suggest>) session.getAttribute("SUGGESTS");
%>

<%@include file="publicModel/head_admin.jsp" %>

<div style="margin-top: 20px; margin-left: 18%;">
    当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> > 查看留言</a>
</div>
<p></p>


<!-- 主体 -->
<div class="main-container">
    <div class="main-container-title"> 实时通信-与用户</div>
    <%@include file="publicModel/chat.jsp"%>

    <div class="main-container-title"> 查看留言</div>

    <div class="content-header">
        <table class="content-table-3" >
            <thead>
            <tr >
                <th style="width: 10%">留言者</th>
                <th style="width: 25%">身份证号</th>
                <th style="width: 65%">留言内容</th>
            </tr>
            </thead>
            <tbody>
            <%for (int i = 0;i < suggests.size();i++){%>
            <tr >
                <td style="width: 10%"> <%=suggests.get(i).getName()%></td>
                <td style="width: 25%"> <%=suggests.get(i).getIdeCard()%></td>
                <td style="width: 62%"> <%=suggests.get(i).getContext()%></td>
            </tr>
            <%}%>
            </tbody>
        </table>
    </div>
</div>

<%@include file="publicModel/button.jsp"%>
</body>
</html>