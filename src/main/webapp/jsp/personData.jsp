<%@ page import="com.itheima.domain.People" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人中心</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-person-data.css">

    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>

</head>
<body>
<%@include file="publicModel/head.jsp" %>
<div style="margin-top: 20px; margin-left: 18%;">
    当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> > 个人中心</a>
</div>
<p></p>

<!-- 主体 -->
<div class="main-container">
    <div class="main-container-title"> 个人中心</div>

    <!-- 个人资料表格 -->
    <div class="person-data">
        <form action="${pageContext.request.contextPath}/update" method="">
            <div class="person-data-1">
                <table border="0;">
                    <tbody>
                    <tr>
                        <td>姓&nbsp;&nbsp;名:</td>
                        <td><input type="text" name="name"
                                   style="text-align: center;font-size: 16px;width: 170px;"
                                   value="${USER_INFORMATION.getName()}"></td>
                    </tr>

                    <tr>
                        <td>性&nbsp;&nbsp;别:</td>
                        <td>
                            <select id="sex" style="width: 170px;text-align: center;font-size: 16px;" name="sex">
                                <option value="0">女</option>
                                <option value="1">男</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td>手机号:</td>
                        <td><input type="text" style="text-align: center" name="telph"
                                   value="${USER_INFORMATION.getTelph()}"></td>
                    </tr>

                    <tr>
                        <td>身份证号:</td>
                        <td><input type="text" id="userId" style="text-align: center" name="ideCard"
                                   onblur="return validateIdCard()" value="${USER_INFORMATION.getIdeCard()}"></td>
                    </tr>

                    <tr>
                        <td>籍&nbsp;&nbsp;贯:</td>
                        <td>
                            <select id="naPla1" style="font-size: 16px;width: 82px; text-align:center;" name="naPla1">
                            </select>
                            <select id="naPla2" style="font-size: 16px;width: 82px; text-align:center;" name="naPla2">
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>现居住地址:</td>
                        <td>
                            <select id="adres1" style="font-size: 16px;width: 82px; text-align:center;" name="adres1">
                            </select>
                            <select id="adres2" style="font-size: 16px;width: 82px; text-align:center;" name="adres2">
                            </select>
                            <br>
                            <!-- <input type="text" name="address3"></td> </tr> -->
                    <tr>
                        <td>邮箱:</td>
                        <td><input type="text" style="text-align: center" name="email"
                                   value="${USER_INFORMATION.getEmail()}"></td>
                    </tr>

                    <tr>
                        <td>学历:</td>
                        <td><select id="xueli" style="width: 170px;text-align: center;font-size: 16px;" name="eduBack">
                            <option value="1">小学</option>
                            <option value="2">初中</option>
                            <option value="3">高中/职高</option>
                            <option value="4">专科</option>
                            <option value="5">本科</option>
                            <option value="6">研究生</option>
                        </select>
                        </td>
                    </tr>

                    <tr>
                        <td>婚姻状况:</td>
                        <td><select id="married" style="width: 170px;text-align: center;font-size: 16px;"
                                    name="isMarried">
                            <option value="0">未婚</option>
                            <option value="1">已婚</option>
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <td>健康状况:</td>
                        <td><select id="health" style="width: 170px;text-align: center;font-size: 16px;" name="Health">
                            <option value="0">健康</option>
                            <option value="1">残疾</option>
                            <option value="2">亚健康</option>
                        </select>
                        </td>
                    </tr>

                    <tr>
                        <td>就业状况:</td>
                        <td><select id="work" style="width: 170px;text-align: center;font-size: 16px;" name="Work">
                            <option value="0">待业</option>
                            <option value="1">就业</option>
                            <option value="2">学生</option>
                            <option value="3">其他情况</option>
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <td>服兵役状况:</td>
                        <td><select id="army" style="width: 170px;text-align: center;font-size: 16px;" name="isArmy">
                            <option value="0">未服兵役</option>
                            <option value="1">已服兵役</option>
                        </select>
                        </td>
                    </tr>
                    <%--                    <tr>--%>
                    <%--                        <td>头像:</td>--%>
                    <%--                        <td><input type="file" name="Headimg"></td>--%>
                    <%--                    </tr>--%>
                    <tr>
                        <td colspan="2">
                            <input type="submit" onclick="return AreYouSure()" value="修改">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
        <form action="">
            <div class="person-data-headImg">

                <img src="${pageContext.request.contextPath}/userHead/${USER_INFORMATION.getIdeCard()}.jpg"
                     style="width: 100px;height: 100px;">

            </div>
            <a href="${pageContext.request.contextPath}/uploadImg.jsp"> <input type="button" value="更换" class="person-data-headImg-1"> </a>
            <%--            <input type="submit" value="提交" class="person-data-headImg-2">--%>
        </form>
        <a href="${pageContext.request.contextPath}/getYourFace"><input type="button" value="face"></a>

    </div>

</div>
<%@include file="publicModel/button.jsp" %>

<script src="${pageContext.request.contextPath}/js/jquery-3.7.1.min.js"></script>
<script src="${pageContext.request.contextPath}/js/forTheCity.js"></script>
<script type="text/javascript">
    $(function () {
        var sex = '<%=((People) session.getAttribute("USER_INFORMATION")).getSex()%>';
        var naPla1 = '<%=((People) session.getAttribute("USER_INFORMATION")).getNaPla1()%>';
        var naPla2 = '<%=((People) session.getAttribute("USER_INFORMATION")).getNaPla2()%>';
        var adres1 = '<%=((People) session.getAttribute("USER_INFORMATION")).getAdres1()%>';
        var adres2 = '<%=((People) session.getAttribute("USER_INFORMATION")).getAdres2()%>';
        var married = '<%=((People) session.getAttribute("USER_INFORMATION")).getIsMarried()%>';
        var army = '<%=((People) session.getAttribute("USER_INFORMATION")).getIsArmy()%>';
        var eduBack = '<%=((People) session.getAttribute("USER_INFORMATION")).getEduBack()%>';
        var work = '<%=((People) session.getAttribute("USER_INFORMATION")).getWork()%>';
        var health = '<%=((People) session.getAttribute("USER_INFORMATION")).getHealth()%>';

        $("#xueli").val(eduBack);
        $("#sex").val(sex);
        $("#married").val(married);
        $("#army").val(army);
        $("#work").val(work);
        $("#health").val(health);

        setProvince("#naPla1");
        setProvince("#adres1")
        $("#naPla1").val(naPla1);
        $("#adres1").val(adres1);

        var taget = getCity(naPla1);
        setOption("#naPla2", taget);
        taget = getCity(adres1);
        setOption("#adres2", taget);
        $("#naPla2").val(naPla2);
        $("#adres2").val(adres2);

        $("#naPla1").change(function () {
            var opt = $("#naPla1").val();
            var arr = getCity(opt);
            $("#naPla2").find("option").remove();
            setOption("#naPla2", arr);
        });
        $("#adres1").change(function () {
            var opt = $("#adres1").val();
            var arr = getCity(opt);
            $("#adres2").find("option").remove();
            setOption("#adres2", arr);
        });

        // 返回弹窗
        var isSuccess = '<%=(session.getAttribute("isSuccess"))%>';
        if (isSuccess != null && isSuccess != "null") {
            alert("更改成功!!!");
        }
    })

    //身份证验证
    function validateIdCard() {
        var idCard = document.getElementById("userId").value;
        var reg = /(^\d{15}$)|(^\d{17}(\d|X|x)$)/;
        if (!reg.test(idCard)) {
            alert("请输入正确的身份证号！");
            return false;
        }
        var provinceCode = idCard.substring(0, 2);
        var birthday = idCard.substring(6, 14);
        if (!isValidProvinceCode(provinceCode)) {
            alert("身份证号地区信息有误，请重新输入！");
            return false;
        }
        if (!isValidDate(birthday)) {
            alert("身份证号出生日期有误，请重新输入！");
            return false;
        }
        return true;
    }

    function isValidProvinceCode(code) {
        // You can add more provinces to this array
        var provinces = ["11", "12", "13", "14", "15", "21", "22", "23", "31", "32", "33", "34", "35", "36", "37", "41", "42", "43", "44", "45", "46", "50", "51", "52", "53", "54", "61", "62", "63", "64", "65"];
        return provinces.includes(code);
    }

    function isValidDate(dateString) {
        var year = parseInt(dateString.substring(0, 4));
        var month = parseInt(dateString.substring(4, 6));
        var day = parseInt(dateString.substring(6, 8));
        var date = new Date(year, month - 1, day);
        return (date.getFullYear() === year && date.getMonth() === month - 1 && date.getDate() === day);
    }
</script>
<%
    if (session.getAttribute("isSuccess") != null) {
        session.removeAttribute("isSuccess");
    }
%>
</body>
</html>