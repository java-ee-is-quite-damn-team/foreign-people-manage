<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>人口流动情况</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-person-data.css">



    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;

        }
    </style>


</head>
<body>
<%@include file="publicModel/head_admin.jsp"%>
<div class="root">

    <div style="margin-top: 20px; margin-left: 18%;">
        当前位置：<a href="${pageContext.request.contextPath}/jsp/index_admin.jsp"> 首页 </a> > 人口流动情况</a>
    </div>
    <p></p>

    <!-- 主体 -->
    <div class="main-container">
        <div class="main-container-title" style="display: inline-block;">人口流动情况 </div>

<%--        <!-- 搜索框 -->--%>
<%--        <div class="search-box" >--%>
<%--            <div class="search">--%>
<%--                <form action="" >--%>
<%--                    <input type="text" style="width: 120px;border-radius: 15px;">--%>
<%--                    <input type="submit" value="搜索"--%>
<%--                           style=" background-color: #fff; color: #333;--%>
<%--								font-size: 16px;font-weight: bold; ">--%>
<%--                </form>--%>
<%--            </div>--%>
<%--        </div>--%>

        <div class="content-header">
            <table class="content-table" style="width: 100%;">
                <thead>
                <tr>
                    <th>近一个月</th>
                    <th>近一年</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>123人</td>
                    <td>1234人</td>

                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
<%@include file="publicModel/button.jsp"%>

</body>
</html>