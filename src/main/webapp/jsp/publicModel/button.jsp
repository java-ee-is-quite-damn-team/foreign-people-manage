<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- 底部 -->
<div class="footer">
    <div class="friendly-link">
        <div class="friendly-link-item friendly-link-font" id="cn-gov-link">
            厦门理工软件工程服务
            <!-- <div class="triangle-icon footer-triangle"></div> -->
        </div>

        <div class="friendly-link-item friendly-link-font" id="province-gov-link">
            软件工程学院网站
            <!-- <div class="triangle-icon footer-triangle"></div> -->
        </div>


        <div class="friendly-link-item friendly-link-font" id="province-child-link">
            厦门理工学院网站
            <!-- <div class="triangle-icon footer-triangle"></div> -->
        </div>
    </div>
    <div class="friendly-link2">
        关于我们：就挺该死的 | 联系我们 | 免责声明<br>
        地址：厦门理工三期宿舍<br>
        电话：11111111<br>
        厦门理工流动人口服务中心版权所有 copyright © 2023-2025
    </div>
</div>
