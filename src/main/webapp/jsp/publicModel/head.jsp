<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div>

    <!-- 导航图 -->
    <div class="logo-container">
        <div class="logo">
            <img class="new-logo" src="${pageContext.request.contextPath}/image/logo.png" alt="">
            <img class="QRcode-img" src="${pageContext.request.contextPath}/image/logo2.png" alt="">
        </div>
    </div>

    <!-- 导航栏 -->
    <div class="nav-container">
        <div class="nav-title">
            <ul>
                <li>
                    <a href="${pageContext.request.contextPath}/jsp/index.jsp">首页</a>
                </li>
                <!-- <li>
                    <a href="#">政府服务</a>

                </li> -->
                <%if(session.getAttribute("USER_INFORMATION")==null){%>
                <li>
                    <a href="${pageContext.request.contextPath}/jsp/mechanism.jsp">社会组织</a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/jsp/train.jsp" >培训服务</a>
                </li>
                <%}%>
                <%if(session.getAttribute("USER_INFORMATION")!=null){%>
                <li>
                    <a href="${pageContext.request.contextPath}/initMarried" >结婚生育信息管理</a>

                    <!-- <div class="triangle-icon"></div> </a>-->
                    <!-- <ul class="nav-menu-2">
                        <li class="nav-menu-2-item">
                            <a  href="#" onclick="getEID('/PublicPlatform/eids/getRenzheng.action')">eID数字身份认证</a>
                        </li>
                        <li class="nav-menu-2-item">
                            <a  href="#">eID数字身份查询</a>
                        </li>
                    </ul> -->
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/carryingPeopleServlet" >携带人口信息管理</a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/jsp/personData.jsp">个人中心</a>
                    <!-- <div class="triangle-icon"></div></a> -->
                    <!-- <ul class="nav-menu-2">
                        <li class="nav-menu-2-item">
                            <a  href="#">位置查询</a>
                        </li>
                        <li class="nav-menu-2-item">
                            <a  href="#"   >信息采集</a>
                        </li>
                    </ul> -->
                </li>

                <li>
                    <a href="${pageContext.request.contextPath}/jsp/message.jsp">留言</a>
                </li>
                <%}%>
                <!-- <div class="nav-box"></div> -->
            </ul>
        </div>
        <!-- 登录点 -->
        <div class="nav-log" style="position: absolute;right:10%;">
            <a href="${pageContext.request.contextPath}/jsp/Login.jsp"   
               style="font-size: 16px; color: #ffffff; ">
                登录</a>
        </div>
    </div>
</div>
<%@ include file="jsInput.jsp" %>