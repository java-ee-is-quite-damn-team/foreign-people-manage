<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>头部</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/fuwu.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/bootstrap-3.4.1/css/bootstrap.css">

    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>


</head>
<body>

<div>
    <%@include file="model_delete.jsp" %>

    <!-- 导航图 -->
    <div class="logo-container">
        <div class="logo">
            <img class="new-logo" src="${pageContext.request.contextPath}/image/logo.png" alt="">
            <img class="QRcode-img" src="${pageContext.request.contextPath}/image/logo2.png" alt="">

        </div>

    </div>

    <!-- 导航栏 -->
    <div class="nav-container">
        <div class="nav-title">
            <ul style="margin: 0px;">
                <li>
                    <a href="${pageContext.request.contextPath}/jsp/index_admin.jsp">公告管理</a>
                </li>
                <!-- <li>
                    <a href="#">政府服务</a>

                </li> -->
                <li>
                    <a href="${pageContext.request.contextPath}/initUserManage">用户管理</a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/dangerUser">人员预警提示</a>
                    <!-- <div class="triangle-icon"></div> </a>-->

                    <!-- <ul class="nav-menu-2">
                        <li class="nav-menu-2-item">
                            <a  href="#" onclick="getEID('/PublicPlatform/eids/getRenzheng.action')">eID数字身份认证</a>
                        </li>
                        <li class="nav-menu-2-item">
                            <a  href="#">eID数字身份查询</a>
                        </li>
                    </ul> -->
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/jsp/popManager.jsp">人口流动情况</a>
                </li>
                <!-- <li>
                    <a href="personData.html" >个人中心</a>  -->
                <!-- <div class="triangle-icon"></div></a> -->

                <!-- <ul class="nav-menu-2">
                    <li class="nav-menu-2-item">
                        <a  href="#">位置查询</a>
                    </li>
                    <li class="nav-menu-2-item">
                        <a  href="#" >信息采集</a>
                    </li>
                </ul> -->

                <!-- </li> -->
                <li>
                    <a href="${pageContext.request.contextPath}/initManage"
                       onclick="getEID('')">查看留言</a>
                </li>
                <!-- <div class="nav-box"></div> -->
            </ul>
        </div>
        <!-- 登录点 -->
        <div class="nav-log" style="position: absolute;right:10%;">
            <a href="${pageContext.request.contextPath}/jsp/Login.jsp"
               style="font-size: 16px; color: #ffffff; ">
                登录</a>
        </div>
    </div>
</div>

<%@ include file="jsInput.jsp" %>
