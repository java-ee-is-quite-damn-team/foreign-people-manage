<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<script src="${pageContext.request.contextPath}/js/jquery-3.7.1.min.js"></script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-3.4.1/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/js/sockjs.min.js"></script>
<script src="${pageContext.request.contextPath}/js/forTheCity.js"></script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-datepicker-1.9.0/js/bootstrap-datepicker.js"></script>
<script src="${pageContext.request.contextPath}/plugins/bootstrap-datepicker-1.9.0/locales/bootstrap-datepicker.zh-CN.min.js"></script>
<%--<script src="${pageContext.request.contextPath}/plugins/bootstrap-3.4.1/js/bootstrap.js"></script>--%>

<script type="text/javascript">
    var websocket;
    $(function () {
        bindBtnConfirmDeleteEvent();
        MyWebSocket();
        $('#send').bind('click', function () {
            send();
        });
    })

    function MyWebSocket() {
        if (`${USER_SESSION}` != null) {
            if ('WebSocket' in window) {
                console.log("此浏览器支持websocket");
                websocket = new WebSocket("ws://127.0.0.1:8080/websocketDemo/" + ${USER_SESSION.id});
            } else if ('MozWebSocket' in window) {
                alert("此浏览器只支持MozWebSocket");
            } else {
                alert("此浏览器只支持SockJS");
            }
            websocket.onopen = function (evnt) {
                $("#tou").html("链接服务器成功!")
            };
            websocket.onmessage = function (evnt) {
                console.log("sb");
                `<c:forEach var="sb" items="${USERS}">`
                if (evnt.data == ${sb.ideCard}) {
                    console.log(${sb.ideCard});
                    console.log(`${sb.name}`)
                    $("#dangerName").text(`${sb.name}`);
                    $("#deleteModal").modal('show');
                }
                `</c:forEach>`
                console.log("sssbbb")
                $("#msg").html(evnt.data + "<br/>" + $("#msg").html());
                $("#message").val("");
            };
            websocket.onerror = function (evnt) {
            };
            websocket.onclose = function (evnt) {
                $("#tou").html("与服务器断开了链接!")
            };

            if (1 == ${USER_INFORMATION.isCrim}) {
                console.log("偷偷向后台发送信息!!!");
                console.log(${USER_INFORMATION.isCrim});
                `<c:forEach var="a" items="${beSend}">`
                console.log(${a.id})
                var myurl = "http://localhost:8080/message/TestWS?userId=" + ${a.id} +"&message="
                    + ${USER_INFORMATION.ideCard};
                console.log(myurl)
                $.ajax({
                    url: myurl,
                    type: "GET",
                    success: function (result) {
                        console.log("找到你了")
                    }
                });
                `</c:forEach>`
            }
        }
    }

    function bindBtnConfirmDeleteEvent() {
        $("#btnConfirmDelete").click(function () {
            $("#deleteModal").modal('hide');
        });
    }

    function textareaEnter(a) {
        if (a == 13) {
            // $("#send").click()
            send();
        }
    }

    function send() {
        if (websocket != null) {
            console.log('进入了');
            var message = "" + ${USER_SESSION.id} +" : " + document.getElementById('message').value;
            `<c:forEach var="a" items="${beSend}">`
            $.ajax({
                url: "http://localhost:8080/message/TestWS?userId=" + ${a.id} +"&message="
                    + message,
                type: "GET",
                success: function (result) {
                    console.log(result);
                }
            });
            `</c:forEach>`
            $.ajax({
                url: "http://localhost:8080/message/TestWS?userId=" + ${USER_SESSION.id} +"&message="
                    + message,
                type: "GET",
                success: function (result) {
                    console.log(result);
                }
            });
        } else {
            alert('未与服务器链接.');
        }
    }
</script>