<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 删除（对话框） -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">

        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <h4>被预警人员出现!!!</h4>
            <p style="margin: 10px 0;"> <span id="dangerName"></span> 出现!!!</p>
            <p style="margin: 10px 0;"> 请尽快联系相关人员进行抓捕!!!</p>
            <p style="text-align: right;">
                <button id="btnConfirmDelete" type="button" class="btn btn-danger">确 定</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取 消</button>
            </p>
        </div>
    </div>
</div>
