<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>培训服务</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-train.css">

    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>

</head>
<body>
<%@include file="publicModel/head.jsp"%>

<div style="margin-top: 20px; margin-left: 18%;">
    当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> > 培训服务</a>
</div>
<br>


<!-- 主体 -->
<div class="main-container">
    <div class="main-container-title"> 培训服务</div>

    <div class="main-container-img">
        <img src="../image/train1.png" alt="">
        <img src="../image/train2.png" alt="">
    </div>

    <div class="block-about-1">
            <span class="about-title">
                    基层卫生政策培训项目
            </span>
        <span class="about-text">
                    国家卫生健康委流动人口服务中心受 <br> 国家卫生健康委基层卫生健康司委托<br> 开展基层卫生政策培训项目。
            </span>
    </div>

    <div class="block-about-2">
            <span class="about-title">
                    流动人口专业岗位技能培训项目
            </span>
        <span class="about-text">
                    流动人口服务中心与全国妇联人才开发培训中心 <br> 联合开展面向家政行业从业的 <br>流动人口岗位技能服务项目。
            </span>
    </div>
</div>

<%@include file="publicModel/button.jsp"%>
</body>
</html>