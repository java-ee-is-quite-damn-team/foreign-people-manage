<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改个人资料</title>


    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-person-data.css">

    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>


</head>
<body>

<%@include file="publicModel/head.jsp"%>

<div style="margin-top: 20px; margin-left: 18%;">
    当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a><a href="">个人中心</a>
    <a href="">修改个人资料</a>
</div>
<p></p>

<!-- 主体 -->
<div class="main-container">
    <div class="main-container-title">个人中心</div>

    <!-- 个人资料表格 -->
    <div class="person-data">
        <form action="" method="">
            <div class="person-data-1">
                <table border="0;">
                    <tbody>
                    <tr>
                        <td>姓&nbsp;&nbsp;名:</td>
                        <td><input type="text" name="userName"></td>
                    </tr>

                    <tr>
                        <td>性&nbsp;&nbsp;别:</td>
                        <td><select style="width: 170px;text-align: center;font-size: 16px;" name="gender">
                            <option>男</option>
                            <option>女</option>
                        </select></td>
                    </tr>

                    <tr>
                        <td>手机号:</td>
                        <td><input type="text" name="phoneNum"></td>
                    </tr>

                    <tr>
                        <td>身份证号:</td>
                        <td><input type="text" name="" id="userId" onblur="return validateIdCard()" value="${param.userId}"></td>
                    </tr>

                    <tr>
                        <td>籍&nbsp;&nbsp;贯:</td>
                        <td>
                            <select style="font-size: 16px;width: 82px;" name="hometown1">
                                <option>福建</option>
                            </select>
                            <select style="font-size: 16px;width: 82px;" name="hometown2">
                                <option>厦门</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>现居住地址:</td>
                        <td>
                            <select style="font-size: 16px;width: 82px;" name="address1">
                                <option>福建</option>
                            </select>
                            <select style="font-size: 16px;width: 82px;" name="address2">
                                <option>厦门</option>
                            </select>
                            <br>
                            <!-- <input type="text" name="address3"></td> </tr> -->
                    <tr>
                        <td>邮箱:</td>
                        <td><input type="text" name="userMail"></td>
                    </tr>

                    <tr>
                        <td>学历:</td>
                        <td><select style="width: 170px;text-align: center;font-size: 16px;" name="educational">
                            <option>小学</option>
                            <option>初中</option>
                            <option>高中/职高</option>
                            <option>专科</option>
                            <option>本科</option>
                            <option>硕士</option>
                            <option>博士</option>
                        </select>
                        </td>
                    </tr>

                    <tr>
                        <td>婚姻状况:</td>
                        <td><select style="width: 170px;text-align: center;font-size: 16px;" name="isMarriage">
                            <option>已婚</option>
                            <option>未婚</option>
                        </select>
                        </td>
                    </tr>

                    <tr>
                        <td>健康状况:</td>
                        <td><select id="health" style="width: 170px;text-align: center;font-size: 16px;" name="isHealth">
                            <option value="0">健康</option>
                            <option value="1">残疾</option>
                        </select>
                        </td>
                    </tr>

                    <tr>
                        <td>就业状况:</td>
                        <td><select id="work" style="width: 170px;text-align: center;font-size: 16px;" name="isWork">
                            <option value="0">待业</option>
                            <option value="1">就业</option>
                            <option value="0">学生</option>
                            <option value="1">其他情况</option>
                        </select>
                        </td>
                    </tr>

                    <tr>
                        <td>服兵役状况:</td>
                        <td><select style="width: 170px;text-align: center;font-size: 16px;" name="isArmy">
                            <option>已服兵役</option>
                            <option>未服兵役</option>
                        </select>
                        </td>
                    </tr>

                    <tr>
                        <td>头像:</td>
                        <td><input type="file" name="userHead"></td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="submit" value="保存"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>

        <form action="">
            <div class="person-data-headImg">
                头像
            </div>
            <input type="button" value="更换" class="person-data-headImg-1">
            <input type="submit" value="提交" class="person-data-headImg-2">
        </form>

    </div>

</div>

<%@include file="publicModel/button.jsp"%>
</body>
<script>
    //身份证验证
    function validateIdCard() {
        var idCard = document.getElementById("userId").value;
        var reg = /(^\d{15}$)|(^\d{17}(\d|X|x)$)/;
        if (!reg.test(idCard)) {
            alert("请输入正确的身份证号！");
            return false;
        }
        var provinceCode = idCard.substring(0, 2);
        var birthday = idCard.substring(6, 14);
        if (!isValidProvinceCode(provinceCode)) {
            alert("身份证号地区信息有误，请重新输入！");
            return false;
        }
        if (!isValidDate(birthday)) {
            alert("身份证号出生日期有误，请重新输入！");
            return false;
        }
        return true;
    }
    function isValidProvinceCode(code) {
        // You can add more provinces to this array
        var provinces = ["11", "12", "13", "14", "15", "21", "22", "23", "31", "32", "33", "34", "35", "36", "37", "41", "42", "43", "44", "45", "46", "50", "51", "52", "53", "54", "61", "62", "63", "64", "65"];
        return provinces.includes(code);
    }
    function isValidDate(dateString) {
        var year = parseInt(dateString.substring(0, 4));
        var month = parseInt(dateString.substring(4, 6));
        var day = parseInt(dateString.substring(6, 8));
        var date = new Date(year, month - 1, day);
        return (date.getFullYear() === year && date.getMonth() === month - 1 && date.getDate() === day);
    }
</script>
</html>