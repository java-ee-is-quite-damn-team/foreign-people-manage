<%@ page import="com.itheima.domain.People" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人中心</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/common-person-data.css">

    <style>
        body {
            background-image: url(${pageContext.request.contextPath}/image/background.png);
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100%;
        }
    </style>

</head>
<body>
<%@include file="/jsp/publicModel/head.jsp" %>
<div style="margin-top: 20px; margin-left: 18%;">
    当前位置：<a href="${pageContext.request.contextPath}/jsp/index.jsp"> 首页 </a> > 头像更改</a>
</div>
<p></p>

<!-- 主体 -->
<div class="main-container">
    <div class="main-container-title"> 头像更改</div>

    <form action="${pageContext.request.contextPath}/UploadServlet" enctype="multipart/form-data" method="post">
        <input type="text" name="uname" readonly="readonly" value="${USER_INFORMATION.getIdeCard()}" style="width: 200px"> <br>
        <input type="file" name="myfile"> <br>
        <a href="${pageContext.request.contextPath}/jsp/personData.jsp">
            <button>提交</button>
        </a>
    </form>
    <br>

</div>

</body>
</html>