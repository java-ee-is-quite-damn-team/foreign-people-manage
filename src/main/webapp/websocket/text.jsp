<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Hater
  Date: 2024/6/15
  Time: 16:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/bootstrap-3.4.1/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/plugins/bootstrap-3.4.1/css/bootstrap-theme.css">
    <script src="${pageContext.request.contextPath}/js/jquery-3.7.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/plugins/bootstrap-3.4.1/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/sockjs.min.js"></script>

    <title>webSocket-用户66</title>

    <script type="text/javascript">
        $(function () {
            var websocket;
            if ('WebSocket' in window) {
                console.log("此浏览器支持websocket");
                websocket = new WebSocket("ws://127.0.0.1:8080/websocketDemo/"+ ${USER_SESSION.id} );
            } else if ('MozWebSocket' in window) {
                alert("此浏览器只支持MozWebSocket");
            } else {
                alert("此浏览器只支持SockJS");
            }
            websocket.onopen = function (evnt) {
                $("#tou").html("链接服务器成功!")
            };
            websocket.onmessage = function (evnt) {
                $("#msg").html($("#msg").html() + "<br/>" + evnt.data);
            };
            websocket.onerror = function (evnt) {
            };
            websocket.onclose = function (evnt) {
                $("#tou").html("与服务器断开了链接!")
            }

            $('#send').bind('click', function () {
                console.log("send")
                send();
            });
            function send() {
                if (websocket != null) {
                    console.log('进入了')
                    var message = "" + ${USER_SESSION.id} + " : " + document.getElementById('message').value;
                    `<c:forEach var="a" items="${beSend}">`
                    $.ajax({
                        url: "http://localhost:8080/message/TestWS?userId="+ ${a.id} +"&message="
                            + message,
                        type: "GET",
                        success: function (result) {
                            console.log(result)
                        }
                    });
                    `</c:forEach>`
                    $.ajax({
                        url: "http://localhost:8080/message/TestWS?userId=" + ${USER_SESSION.id} +"&message="
                            + message,
                        type: "GET",
                        success: function (result) {
                            console.log(result)
                        }
                    });
                } else {
                    alert('未与服务器链接.');
                }
            }
        });
    </script>

</head>
<body>

<div class="page-header" id="tou">
    webSocket多终端聊天测试
</div>
<div class="well" id="msg"></div>
<div class="col-lg">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="发送信息..." id="message">
        <span class="input-group-btn">
           <button class="btn btn-default" type="button" id="send">发送</button>
        </span>
    </div>
</div>

</body>
</html>
